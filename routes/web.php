<?php

Route::get('/', 'Auth\LoginController@showLoginForm');

Route::post('login', 'Auth\LoginController@login')->name('login');

Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('dashboard', 'DashBoardController@index')->name('dashboard');
//
// Rutas correspondientes a los catálogos
//
Route::resource('areas', 'AreaController');

Route::resource('asignaturas', 'SubjectController');

Route::resource('partidas', 'DepartureController');

Route::resource('unidades', 'UnitController');

Route::resource('categorias', 'CategoryController');

Route::resource('marcas', 'BrandController');

Route::resource('modelos', 'ExemplarController');

Route::resource('tipos', 'TypeController');

Route::resource('materiales', 'MaterialController');

Route::resource('stock', 'StockController');
//
// Rutas para las requisiciones
//
Route::get('requisiciones', 'RequisitionController@index')->name('requisiciones.index');

Route::get('requisiciones/crear', 'RequisitionController@create')->name('requisiciones.crear');

Route::post('requisiciones/guardar', 'RequisitionController@store')->name('requisiciones.guardar');

Route::get('requisiciones/{requisicion}', 'RequisitionController@assign')->name('requisiciones.asignar');

Route::post('requisiciones/detalles', 'RequisitionController@storeDetails')->name('requisiciones.detalles.guardar');

Route::delete('requisiciones/{requisicion}', 'RequisitionController@destroy')->name('requisiciones.eliminar');

Route::get('requisiciones/imprimir/{requisicion}', 'RequisitionController@excel')->name('requisiciones.imprimir');

Route::get('requisiciones/enviar/{requisicion}', 'RequisitionController@sendRequisition')->name('requisiciones.enviar');

Route::get('requisiciones/recepcionar/{requisicion}', 'RequisitionController@receive')->name('requisiciones.recepcionar');

Route::post('requisiciones/recepcionar/detalles', 'RequisitionController@storeDetailsReceive')->name('recepcionar.guardar');

Route::get('requisiciones/{requisicion}/detalles/{detail}', 'RequisitionController@details')->name('requisiciones.detalles.almacen');

Route::post('requisiciones/detalles/almacen', 'RequisitionController@pullItemToStock')->name('requisiciones.almacen.guardar');

Route::get('requisiciones/download/{requisicion}', 'RequisitionController@downloadQR')->name('requisiciones.download');
//
// Rutas para solicitud de laboratorio
//
Route::get('solicitudes', 'RequestLaboratoriesController@index')->name('solicitudes.index');

Route::get('solicitudes/crear', 'RequestLaboratoriesController@create')->name('solicitudes.crear');

Route::post('solicitudes', 'RequestLaboratoriesController@addEvent')->name('solicitudes.añadir');

Route::delete('solicitudes/{solicitud}', 'RequestLaboratoriesController@destroy')->name('solicitudes.eliminar');

Route::get('solicitudes/aceptar/{requisicion}', 'RequestLaboratoriesController@acceptRequest')->name('solicitudes.aceptar');
//
// Rutas para prestamo de materiales
//
Route::get('prestamos', 'LoanController@index')->name('prestamos.index');

Route::get('prestamos/crear', 'LoanController@create')->name('prestamos.crear');

Route::delete('prestamos/{prestamo}', 'LoanController@destroy')->name('prestamos.eliminar');

Route::post('prestamos/guardar', 'LoanController@store')->name('prestamos.guardar');

Route::get('prestamos/{prestamo}', 'LoanController@assign')->name('prestamos.asignar');

Route::post('prestamos/detalles', 'LoanController@storeDetails')->name('prestamos.detalles.guardar');

Route::get('prestamos/enviar/{prestamo}', 'LoanController@sendLoan')->name('prestamos.enviar');

Route::get('prestamos/ver/{prestamo}', 'LoanController@seeLoan')->name('prestamos.ver');

Route::get('prestamos/devolver/{prestamo}', 'LoanController@sendBack')->name('prestamos.devolver');

Route::get('prestamos/recibir/{prestamo}', 'LoanController@receive')->name('prestamos.recibir');

Route::post('prestamos/recibir/almacenar', 'LoanController@stockMaterials')->name('prestados.almacenar');

Route::post('prestamos/ver/guardar', 'LoanController@storeLoanMaterials')->name('prestados.guardar');

Route::post('prestamos/devolver/guardar', 'LoanController@returnMaterial')->name('prestados.devolver.guardar');

Route::get('prestamos/prestados/{prestamo}', 'LoanController@seeLoanApproved')->name('aprobados.ver');

//Route::get('requisiciones/{id}', 'RequisitionController@show')->name('products.show')->middleware('permission:products.show');

//Route::get('requisiciones/editar/{id?}',['as'=>'editarrequisicion', 'uses' => 'RequisitionController@showRequisition'])->where('id_grupo', "[0-9]+");