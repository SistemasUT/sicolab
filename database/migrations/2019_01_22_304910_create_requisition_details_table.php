<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequisitionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requisition_details', function (Blueprint $table) {
            // Definir el formato de la BD
            $table->engine = 'innoDB';

            $table->increments('id');
            $table->string('component', 100)->nullable();
            $table->integer('amount');
            $table->integer('receive')->nullable();
            $table->integer('storage')->nullable();
            $table->string('description', 300)->nullable();
            $table->integer('departure_id')->unsigned();
            $table->integer('requisition_id')->unsigned();
            $table->integer('material_id')->unsigned()->nullable();
            $table->integer('status_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('departure_id')
                  ->references('id')->on('departures')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');

            $table->foreign('requisition_id')
                  ->references('id')->on('requisitions')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');

            $table->foreign('material_id')
                  ->references('id')->on('materials')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');

            $table->foreign('status_id')
                  ->references('id')->on('statuses')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');
        });

        Schema::table('requisition_details', function(Blueprint $table){
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('requisition_details', function(Blueprint $table){
        $table->dropForeign('requisition_details_departure_id_foreign');
        $table->dropForeign('requisition_details_requisition_id_foreign');
        $table->dropForeign('requisition_details_material_id_foreign');
        $table->dropForeign('requisition_details_status_id_foreign');
      });
      
      Schema::dropIfExists('requisition_details');
    }
}
