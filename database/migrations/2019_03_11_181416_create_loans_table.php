<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            // Definir el formato de la BD
            $table->engine = 'innoDB';

            $table->increments('id');
            $table->timestamp('starts');
            $table->timestamp('ends');
            $table->string('event', 100);
            $table->string('description', 400)->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('status_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');

            $table->foreign('status_id')
                  ->references('id')->on('statuses')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');
        });

        Schema::table('loans', function(Blueprint $table){
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loans', function(Blueprint $table){
            $table->dropForeign('loans_user_id_foreign');
            $table->dropForeign('loans_status_id_foreign');
        });

        Schema::dropIfExists('loans');
    }
}
