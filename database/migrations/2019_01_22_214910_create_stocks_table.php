<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            // Definir el formato de la BD
            $table->engine = 'innoDB';

            $table->increments('id');
            $table->string('serial', 100)->nullable();
            $table->string('stock', 100)->nullable();
            $table->string('qr', 100)->nullable();
            $table->date('expiration')->nullable();
            $table->integer('requisition_id')->unsigned()->nullable();
            $table->integer('material_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('requisition_id')
                  ->references('id')->on('requisitions')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');

            $table->foreign('material_id')
                  ->references('id')->on('materials')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');
        });

        Schema::table('stocks', function(Blueprint $table){
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stocks', function(Blueprint $table){
            $table->dropForeign('stocks_requisition_id_foreign');
            $table->dropForeign('stocks_material_id_foreign');
        });

        Schema::dropIfExists('stocks');
    }
}
