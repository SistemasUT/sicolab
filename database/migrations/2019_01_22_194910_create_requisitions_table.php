<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequisitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requisitions', function (Blueprint $table) {
            // Definir el formato de la BD
            $table->engine = 'innoDB';

            $table->increments('id');
            $table->string('concept', 200);
            $table->string('event', 200);
            $table->string('folio', 100)->nullable();
            $table->integer('area_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('status_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('area_id')
                  ->references('id')->on('areas')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');

            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');

            $table->foreign('status_id')
                  ->references('id')->on('statuses')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');
        });

        Schema::table('requisitions', function(Blueprint $table){
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requisitions', function(Blueprint $table){
            $table->dropForeign('requisitions_area_id_foreign');
            $table->dropForeign('requisitions_user_id_foreign');
            $table->dropForeign('requisitions_status_id_foreign');
        });

        Schema::dropIfExists('requisitions');
    }
}
