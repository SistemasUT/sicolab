<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_records', function (Blueprint $table) {
            // Definir el formato de la BD
            $table->engine = 'innoDB';

            $table->increments('id');
            $table->integer('loan_id')->unsigned();
            $table->integer('material_id')->unsigned();
            $table->integer('amount');
            $table->integer('returned')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('loan_id')
                  ->references('id')->on('loans')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');

            $table->foreign('material_id')
                  ->references('id')->on('materials')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loan_records', function(Blueprint $table){
            $table->dropForeign('loan_records_loan_id_foreign');
            $table->dropForeign('loan_records_material_id_foreign');
        });

        Schema::dropIfExists('loan_records');
    }
}
