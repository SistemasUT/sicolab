<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestLaboratoriesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_laboratories_details', function (Blueprint $table) {
            // Definir el formato de la BD
            $table->engine = 'innoDB';

            $table->increments('id');
            $table->integer('request_laboratory_id')->unsigned();
            $table->integer('subject_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->date('date');
            $table->time('starts');
            $table->time('ends');
            $table->integer('status_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('request_laboratory_id')
                  ->references('id')->on('request_laboratories')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');

            $table->foreign('subject_id')
                  ->references('id')->on('subjects')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');

            $table->foreign('category_id')
                  ->references('id')->on('categories')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');

            $table->foreign('status_id')
                  ->references('id')->on('statuses')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_laboratories_details', function(Blueprint $table){
            $table->dropForeign('request_laboratories_details_request_laboratory_id_foreign');
            $table->dropForeign('request_laboratories_details_subject_id_foreign');
            $table->dropForeign('request_laboratories_details_category_id_foreign');
            $table->dropForeign('request_laboratories_details_status_id_foreign');
        });

        Schema::dropIfExists('request_laboratories_details');
    }
}
