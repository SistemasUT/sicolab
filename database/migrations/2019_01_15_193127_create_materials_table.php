<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials', function (Blueprint $table) {
            // Definir el formato de la BD
            $table->engine = 'innoDB';

            $table->increments('id');
            //$table->string('name', 100);
            $table->string('image', 300);
            $table->string('model', 100);
            $table->string('description', 300)->nullable();
            $table->integer('type_id')->unsigned();
            $table->integer('unit_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('brand_id')->unsigned();
            //$table->string('model', 100);
            //$table->integer('exemplar_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('type_id')
                  ->references('id')->on('types')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');

            $table->foreign('unit_id')
                  ->references('id')->on('units')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');

            $table->foreign('category_id')
                  ->references('id')->on('categories')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');

            $table->foreign('brand_id')
                  ->references('id')->on('brands')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');

            /*$table->foreign('exemplar_id')
                  ->references('id')->on('exemplars')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');*/
        });

        Schema::table('materials', function(Blueprint $table){
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('materials', function(Blueprint $table){
            $table->dropForeign('materials_type_id_foreign');
            $table->dropForeign('materials_unit_id_foreign');
            $table->dropForeign('materials_category_id_foreign');
            $table->dropForeign('materials_brand_id_foreign');
            //$table->dropForeign('materials_exemplar_id_foreign');
        });

        Schema::dropIfExists('materials');
    }
}
