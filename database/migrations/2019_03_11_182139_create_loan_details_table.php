<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_details', function (Blueprint $table) {
            // Definir el formato de la BD
            $table->engine = 'innoDB';

            $table->increments('id');
            $table->integer('loan_id')->unsigned();
            $table->integer('material_id')->unsigned();
            $table->integer('amount');
            $table->integer('given')->nullable();
            $table->integer('receive')->nullable();
            $table->integer('status_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('loan_id')
                  ->references('id')->on('loans')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');

            $table->foreign('material_id')
                  ->references('id')->on('materials')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');

            $table->foreign('status_id')
                  ->references('id')->on('statuses')
                  ->onUpdate('CASCADE')
                  ->onDelete('NO ACTION');
        });

        Schema::table('loan_details', function(Blueprint $table){
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loan_details', function(Blueprint $table){
            $table->dropForeign('loan_details_loan_id_foreign');
            $table->dropForeign('loan_details_material_id_foreign');
            $table->dropForeign('loan_details_status_id_foreign');
        });

        Schema::dropIfExists('loan_details');
    }
}
