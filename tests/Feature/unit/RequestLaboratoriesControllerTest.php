<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     *
     * @test
     */

    // Función en PHPUnit que crea una solicitud de laboratorio
    public function create_request_of_laboratorie()
    {
        // Se visita la ruta /solicitudes/crear
        $this->visit('/solicitudes/crear');
        // Se ingresa una fecha de inicio
        $this->type('2019-04-03 07:00:00','inicio');
        // Se ingresa una fecha de fin
        $this->type('2019-04-03 10:00:00','fin');
        // Se presiona el botón solicitar
        $this->press('Solicitar');
        // Se verifica que dicho dato se encuentre en la tabla request_laboratories_details
        $this->seeInDatabase('request_laboratories_details', [
            'starts' => '2019-04-03 07:00:00',
            'ends' => '2019-04-03 10:00:00'
        ]);
    }
}
