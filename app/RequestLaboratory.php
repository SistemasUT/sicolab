<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestLaboratory extends Model
{
    protected $fillable = ['user_id', 'status_id', 'created_at'];

    public function requestLaboratoriesDetail()
    {
        return $this->belongsTo('App\RequestLaboratoriesDetail');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }
}
