<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = [
		'serial',
		'stock',
		'expiration',
		'requisition_id',
		'material_id'
	];

    public function material()
    {
        return $this->belongsTo('App\Material');
    }
}
