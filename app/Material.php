<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
	protected $fillable = [
		'name',
		'image',
        'model',
		'description',
        'type_id',
        'unit_id',
		'category_id',
		'brand_id'
	];

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

	public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function stock()
    {
        return $this->hasOne('App\Stock');
    }

    public function requisitionDetail()
    {
        return $this->hasMany('App\requisitionDetail');
    }
}
