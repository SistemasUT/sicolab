<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departure extends Model
{
    protected $fillable = [
		'name',
	    'description'
	];

	public function requisition()
    {
        return $this->hasOne('App\Requisition');
    }
}
