<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $fillable =['starts', 'ends', 'event', 'description', 'user_id', 'status_id'];

    public function user(){
    	
        return $this->belongsTo('App\User');
    }

    public function status(){
    	
        return $this->belongsTo('App\Status');
    }
}
