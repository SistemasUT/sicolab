<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public function requisition()
    {
        return $this->hasOne('App\Requisition');
    }

    public function requestLaboratory()
    {
        return $this->hasOne('App\RequestLaboratory');
    }

    public function requestLaboratoryDetail()
    {
        return $this->hasOne('App\RequestLaboratoriesDetail');
    }
    public function loan(){

        return $this->hasOne('App\Loan');
    }
}
