<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanRecord extends Model
{
    protected $fillable =['loan_id', 'material_id', 'amount'];
}
