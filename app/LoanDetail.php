<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanDetail extends Model
{
        protected $fillable =['loan_id', 'material_id', 'amount'];
}
