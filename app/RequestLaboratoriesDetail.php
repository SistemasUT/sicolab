<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestLaboratoriesDetail extends Model
{
    protected $fillable = ['request_laboratory_id', 'subject_id', 'category_id', 'date', 'starts', 'ends', 'status_id'];

    public function requestLaboratory()
    {
        return $this->hasMany('App\RequestLaboratory');
    }

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }
}
