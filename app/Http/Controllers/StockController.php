<?php

namespace App\Http\Controllers;

use App\Stock;
use App\Category;
use App\Material;
//use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class StockController extends Controller
{
	public function __construct(){
    	$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
    	
        $stocks = Stock::orderBy('material_id', 'asc')->latest()->paginate(7);

    	/*$stocks = DB::table('stocks')
        	->leftJoin('materials', 'stocks.material_id', '=', 'materials.id')
            ->leftJoin('types', 'materials.type_id', '=', 'types.id')
            ->leftJoin('units', 'materials.unit_id', '=', 'units.id')
        	->leftJoin('categories', 'materials.category_id', '=', 'categories.id')
        	->leftJoin('brands', 'materials.brand_id', '=', 'brands.id')
        	->select('stocks.*', 'materials.model', 'types.name as type', 'units.name as unit', 'brands.name as brand')
        	->orderBy('categories.id')
            ->get();*/


        /*$stocks = DB::table('stocks')
            ->leftJoin('materials', 'stocks.material_id', '=', 'materials.id')
            ->leftJoin('types', 'materials.type_id', '=', 'types.id')
            ->leftJoin('units', 'materials.unit_id', '=', 'units.id')
            ->leftJoin('categories', 'materials.category_id', '=', 'categories.id')
            ->leftJoin('brands', 'materials.brand_id', '=', 'brands.id')
            ->select('stocks.*', 'materials.*', 'types.name as type', 'units.name as unit', 'categories.name as category', 'brands.name as brand')
            ->where('categories.id', '=', $id)
            ->get();*/

        return view('stock.index', compact('categories', 'stocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $materials = Material::all();

        return view('stock.crear', compact('categories','materials'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        
        $request->validate([
            'serial'=>'required|max:100',
        ]);

        $material = new Stock([
            'serial' => $request->get('serial'),
            'expiration' => $request->get('expiration'),
            'material_id' => $request->get('material_id')
        ]);

        $material->save();
        
        return redirect('/stock')
        ->with('successed', 'El material se registró exitasamente en el stock!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $itemStock = Stock::find($id);
        /*$stocks = DB::table('stocks')
            ->leftJoin('materials', 'stocks.material_id', '=', 'materials.id')
            ->leftJoin('types', 'materials.type_id', '=', 'types.id')
            ->leftJoin('units', 'materials.unit_id', '=', 'units.id')
            ->leftJoin('categories', 'materials.category_id', '=', 'categories.id')
            ->leftJoin('brands', 'materials.brand_id', '=', 'brands.id')
            ->select('stocks.*', 'materials.*', 'types.name as type', 'units.name as unit', 'categories.name as category', 'brands.name as brand')
            ->where('categories.id', '=', $id)
            ->get();*/

        return view('stock.show',compact('itemStock'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $material = Stock::find($id);
        $categories = Category::all();
        $brands = Brand::all();
        $exemplars = Exemplar::all();

        return view('stock.editar', compact('material','categories','brands','exemplars'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|max:80',
            'key'=>'required|max:50',
            'code'=>'required|integer|min:1|max:99999999999',
            'description'=> 'max:300',
            'category_id'=>'required',
            'brand_id'=>'required',
            'exemplar_id'=>'required'
        ]);

        $material = Stock::find($id);
        $material->name = $request->get('name');
        $material->key = $request->get('key');
        $material->code = $request->get('code');
        $material->description = $request->get('description');
        $material->category_id = $request->get('category_id');
        $material->brand_id = $request->get('brand_id');
        $material->exemplar_id = $request->get('exemplar_id');
        $material->save();

        return redirect('/stock')
            ->with('successed', 'El material se modificó exitasamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $material = Stock::find($id);
        $material->delete();

        return redirect('/stock')->with('successed', 'El material se eliminó exitasamente del stock!');
	}
}
