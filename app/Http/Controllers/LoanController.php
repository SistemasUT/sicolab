<?php

namespace App\Http\Controllers;

use Auth;
use App\Loan;
use App\Departure;
use App\LoanDetail;
use App\LoanRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoanController extends Controller
{
	public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

    	if(Auth::user()->id == 3 || Auth::user()->id == 4){

    		$loans = Loan::where('user_id', Auth::user()->id)->get();

    		return view('prestamos.index', compact('loans'));

    	}elseif(Auth::user()->id == 2) {

    		$loans = Loan::where('status_id', 2)
                ->orWhere('status_id', 4)
    			->orWhere('status_id', 6)
    			->orWhere('status_id', 7)
    			->orWhere('status_id', 8)
    			->get();

    		return view('prestamos.indexalmacen', compact('loans'));
    	}
    }

    public function create(){

    	return view('prestamos.crear');
    }

    public function store(Request $request){

    	$request->validate([
    		'evento'=>'required|max:100',
            'inicio'=>'required',
            'fin'=>'required',
            'descripcion'=>'max:400',
        ]);

        $loan = new Loan([
            'starts' => $request->get('inicio'),
            'ends' => $request->get('fin'),
            'event' => $request->get('evento'),
            'description'=> $request->get('descripcion'),
            'user_id' => Auth::user()->id,
            'status_id' => 5
        ]);

        $loan->save();
        
        return redirect('/prestamos')
        ->with('successed', 'La solicitud de préstamo se registró exitasamente!');

    }

    public function assign($id){

    	$loanID = $id;

        $stocks = DB::table('materials AS M')
            ->leftJoin('types AS T', 'M.type_id', '=', 'T.id')
            ->leftJoin('units AS U', 'M.unit_id', '=', 'U.id')
            ->leftJoin(DB::raw('(SELECT stocks.material_id, COUNT(stocks.material_id) stock FROM stocks GROUP BY stocks.material_id) AS S'),
                function($leftJoin){
                    $leftJoin->on('M.id', '=', 'S.material_id');
                }
            )
            ->leftJoin(DB::raw('(SELECT loan_details.material_id, SUM(loan_details.amount) loan FROM loan_details GROUP BY loan_details.material_id ) AS L'),
                function($leftJoin){
                    $leftJoin->on('M.id', '=', 'L.material_id');
                }
            )
            ->select('S.material_id AS id', 'T.name AS name', 'U.name AS unit', 'S.stock', 'L.loan', DB::raw('S.stock - L.loan AS available'))
            ->having('stock', '>=', 1)
            ->get();

        /*$stocks = DB::table('materials AS M')
            ->leftJoin('types AS T', 'M.type_id', '=', 'T.id')
            ->leftJoin('units AS U', 'M.unit_id', '=', 'U.id')
            ->leftJoin(DB::raw('(SELECT stocks.material_id, COUNT(stocks.material_id) stock FROM stocks GROUP BY stocks.material_id) AS S'),
                function($leftJoin){
                    $leftJoin->on('M.id', '=', 'S.material_id');
                }
            )
            ->leftJoin(DB::raw('(SELECT loan_details.material_id, SUM(loan_details.amount) loan, SUM(loan_details.given) returned FROM loan_details GROUP BY loan_details.material_id ) AS L'),
                function($leftJoin){
                    $leftJoin->on('M.id', '=', 'L.material_id');
                }
            )
            ->select('S.material_id AS id', 'T.name AS name', 'U.name AS unit', 'S.stock', 'L.loan', DB::raw('(S.stock - L.loan) + L.returned AS available'))
            ->having('stock', '>=', 1)
            ->get();*/

        //dd($stocks);

        $details = DB::table('materials AS M')
            ->leftJoin('types AS T', 'M.type_id', '=', 'T.id')
            ->leftJoin('units AS U', 'M.unit_id', '=', 'U.id')
            ->leftJoin(DB::raw('(SELECT stocks.material_id, COUNT(stocks.material_id) stock FROM stocks GROUP BY stocks.material_id) AS S'),
                function($leftJoin){
                    $leftJoin->on('M.id', '=', 'S.material_id');
                }
            )
            ->leftJoin(DB::raw('(SELECT loan_details.*, SUM(loan_details.amount) loan FROM loan_details WHERE loan_details.loan_id = '.$id.' GROUP BY loan_details.id ) AS L'),
                function($leftJoin){
                    $leftJoin->on('M.id', '=', 'L.material_id');
                }
            )
            ->select('L.id', 'L.loan_id', 'S.material_id', 'T.name AS material', 'U.name AS unit', 'S.stock', 'L.loan', DB::raw('S.stock - L.loan AS available'))
            ->whereNotNull('L.id')
            ->having('stock', '>=', 1)
            ->get();

        return view('prestamos.asignar', compact('loanID','stocks', 'details'));
    }

    public function seeLoan($id){

        $loan = Loan::find($id);

        if(Auth::user()->id == 3 || Auth::user()->id == 4) {
            if($loan->status_id == 4){
                $details = LoanRecord::where('loan_id', $id);

                $details = DB::table('loan_records')
                    ->leftJoin('materials', 'loan_records.material_id', '=', 'materials.id')
                    ->leftJoin('types', 'materials.type_id', '=', 'types.id')
                    ->leftJoin('units', 'materials.unit_id', '=', 'units.id')
                    ->select('loan_records.*', 'types.name as material', 'units.name as unit')
                    ->where('loan_records.loan_id', '=', $id)
                    ->get();

                return view('prestamos.ver',compact('id','details'));
            }else{
                $details = DB::table('loan_details')
                    ->leftJoin('materials', 'loan_details.material_id', '=', 'materials.id')
                    ->leftJoin('types', 'materials.type_id', '=', 'types.id')
                    ->leftJoin('units', 'materials.unit_id', '=', 'units.id')
                    ->select('loan_details.*', 'types.name as material', 'units.name as unit')
                    ->where('loan_details.loan_id', '=', $id)
                    ->get();

                return view('prestamos.ver',compact('id','details'));
            }
        }elseif(Auth::user()->id == 2){
            if($loan->status_id == 4){
                $loanRecord = LoanRecord::where('loan_id', $id);

                $details = DB::table('loan_records')
                    ->leftJoin('materials', 'loan_records.material_id', '=', 'materials.id')
                    ->leftJoin('types', 'materials.type_id', '=', 'types.id')
                    ->leftJoin('units', 'materials.unit_id', '=', 'units.id')
                    ->select('loan_records.*', 'types.name as material', 'units.name as unit')
                    ->where('loan_records.loan_id', '=', $id)
                    ->get();

                return view('prestamos.ver',compact('id','details'));
            }else{
                $details = DB::table('loan_details')
                    ->leftJoin('materials', 'loan_details.material_id', '=', 'materials.id')
                    ->leftJoin('types', 'materials.type_id', '=', 'types.id')
                    ->leftJoin('units', 'materials.unit_id', '=', 'units.id')
                    ->leftJoin('statuses', 'loan_details.status_id', '=', 'statuses.id')
                    ->select('loan_details.*', 'types.name as material', 'units.name as unit', 'statuses.name as status')
                    ->where('loan_details.loan_id', '=', $id)
                    ->get();

                return view('prestamos.veralmacen',compact('id','details'));
            }
        }
    }

    public function storeLoanMaterials(Request $request){

        $loan_id = $request->loan_id;
        $id = $request->id;
        $loan = $request->given;
        $status_id = $request->status_id;
        $count = count($id);
        $ids = array();
        $statuses = array();

        if(!empty($status_id)) {
            foreach($status_id as $key => $status){
                $idStatus = explode("_", $status);

                array_push($ids, $idStatus[0]);
                array_push($statuses, $idStatus[1]);
            }
        }

        for($i = 0; $i < $count; $i++){
            $loanDetail = LoanDetail::find($id[$i]);

            $loanDetail->given = $loan[$i];

            if(empty($status_id)){
                $loanDetail->status_id = NULL;
            }elseif(in_array($id[$i], $ids)){
                $index = array_search($id[$i], $ids);
                $loanDetail->status_id = $statuses[$index];
            }else{
                $loanDetail->status_id = NULL;
            }
            
            $loanDetail->save();
        }

        return redirect('/prestamos/ver/'.$loan_id)->with('successed', 'Materiales modificados exitosamente!');
    }

    public function seeLoanApproved($id){
        $details = DB::table('loan_details')
            ->leftJoin('materials', 'loan_details.material_id', '=', 'materials.id')
            ->leftJoin('types', 'materials.type_id', '=', 'types.id')
            ->leftJoin('units', 'materials.unit_id', '=', 'units.id')
            ->leftJoin('statuses', 'loan_details.status_id', '=', 'statuses.id')
            ->select('loan_details.*', 'types.name as material', 'units.name as unit', 'statuses.name as status')
            ->where('loan_details.loan_id', '=', $id)
            ->get();

        return view('prestamos.prestamoalmacen',compact('id','details'));
    }


    public function returnMaterial(Request $request){
        $loan_id = $request->id;
        $id = $request->details_id;
        $material_id = $request->stock_id;
        $loan = $request->given;
        $status_id = $request->status_id;
        $count = count($id);

        for($i = 0; $i < $count; $i++){

            $loanDetail = LoanDetail::find($id[$i]);

            $loanDetail->receive = $loan[$i];
            
            $loanDetail->save();
        }

        $loan = Loan::find($loan_id);

        $loan->status_id = 7;

        $loan->save();

        return redirect('/prestamos')->with('successed', 'Materiales devueltos exitosamente!');
    }

    public function sendBack($id){
    	
    	$details = DB::table('loan_details')
	    	->leftJoin('materials', 'loan_details.material_id', '=', 'materials.id')
	    	->leftJoin('types', 'materials.type_id', '=', 'types.id')
	        ->leftJoin('units', 'materials.unit_id', '=', 'units.id')
	        ->select('loan_details.*', 'types.name as material', 'units.name as unit')
	        ->where('loan_details.loan_id', '=', $id)
	        ->get();

    	return view('prestamos.devolver', compact('id', 'details'));
    }

    public function receive($id){

    	$details = DB::table('loan_details')
	    	->leftJoin('materials', 'loan_details.material_id', '=', 'materials.id')
	    	->leftJoin('types', 'materials.type_id', '=', 'types.id')
	        ->leftJoin('units', 'materials.unit_id', '=', 'units.id')
	        ->select('loan_details.*', 'types.name as material', 'units.name as unit')
	        ->where('loan_details.loan_id', '=', $id)
	        ->get();

	    return view('prestamos.recibir', compact('id', 'details'));
    }


    public function stockMaterials(Request $request){
        $loan_id = $request->loan_id;
        $id = $request->details_id;
        $material_id = $request->stock_id;
        $amount = $request->amount;
        $returned = $request->returned;
        $count = count($id);

        for($i = 0; $i < $count; $i++){

            $loanRecord = new LoanRecord([
                'loan_id' => $loan_id,
                'material_id' => $material_id[$i],
                'amount' => $amount[$i],
                'returned' => $returned[$i]
            ]);

            $loanRecord->save();

            $loanDetail = LoanDetail::find($id[$i]);
            
            $loanDetail->delete();
        }

        $loan = Loan::find($loan_id);

        $loan->status_id = 4;

        $loan->save();

        return redirect('/prestamos')->with('successed', 'Materiales almacenados exitosamente!');
    }

    public function storeDetails(Request $request){

        $loan_id = $request->loan_id;

        DB::table('loan_details')->where('loan_id', '=', $loan_id)->delete();

        $material_id = $request->stock_id;
        $amount = $request->amount;

        $count = count($material_id);

        for($i = 0; $i < $count; $i++){
            $loanDetail = new LoanDetail();
            $loanDetail->loan_id = $loan_id;
            $loanDetail->material_id = $material_id[$i];
            $loanDetail->amount = $amount[$i];
            $loanDetail->save();
        }

        return redirect('/prestamos/'.$loan_id)->with('successed', 'Materiales modificados exitosamente!');
    }

    public function sendLoan($id){

        $loan = Loan::find($id);

        if($loan->status_id == 5){

        	$loan->status_id = 2;

        	$loan->save();
    	}elseif($loan->status_id == 2){

    		$loan->status_id = 6;

        	$loan->save();
    	}elseif($loan->status_id == 6){

    		$loan->status_id = 7;

        	$loan->save();
    	}else{

    		$loan->status_id = 8;

        	$loan->save();
    	}
        
        return redirect('/prestamos')
        ->with('successed', 'La solicitud se envió exitasamente!');
    }

    public function destroy($id){

        $loanDetail = LoanDetail::where('loan_id', $id);
        $loanDetail->delete();

        $loan = Loan::find($id);
        $loan->delete();

        return redirect('/prestamos')->with('successed', 'La solicitud de préstamo se eliminó exitasamente!');
    }
}
