<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Area;

class AreaController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$areas = Area::all();
    	$areas = Area::orderBy('id', 'asc')->latest()->paginate(7);

        return view('areas.index', compact('areas'))->with('i', (request()->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('areas.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'name'=>'required|max:80',
        'description'=> 'max:300'
      ]);
      $area = new Area([
        'name' => $request->get('name'),
        'description'=> $request->get('description')
      ]);
      $area->save();
      return redirect('/areas')
      	->with('successed', 'La marca se registró exitasamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$area = Area::find($id);

        return view('areas.show',compact('area'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $area = Area::find($id);

        return view('areas.editar', compact('area'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$request->validate([
        	'name'=>'required|max:80',
        	'description'=> 'max:300'
      	]);

      	$area = Area::find($id);
    	$area->name = $request->get('name');
    	$area->description = $request->get('description');
      	$area->save();

      	return redirect('/areas')
      		->with('successed', 'La marca se modificó exitasamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$area = Area::find($id);
    	$area->delete();

    	return redirect('/areas')->with('successed', 'La marca se eliminó exitasamente!');
	}
}
