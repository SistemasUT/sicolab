<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model;

class ModelController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$models = Model::all();
    	$models = Model::orderBy('id', 'asc')->latest()->paginate(7);

        return view('modelos.index', compact('models'))->with('i', (request()->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelos.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'name'=>'required|max:80',
        'description'=> 'max:300'
      ]);
      $brand = new Model([
        'name' => $request->get('name'),
        'description'=> $request->get('description')
      ]);
      $brand->save();
      return redirect('/modelos')
      	->with('successed', 'La marca se registró exitasamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$brand = Model::find($id);

        return view('modelos.show',compact('brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Model::find($id);

        return view('modelos.editar', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$request->validate([
        	'name'=>'required|max:80',
        	'description'=> 'max:300'
      	]);

      	$brand = Model::find($id);
    	$brand->name = $request->get('name');
    	$brand->description = $request->get('description');
      	$brand->save();

      	return redirect('/modelos')
      		->with('successed', 'La marca se modificó exitasamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$brand = Model::find($id);
    	$brand->delete();

    	return redirect('/modelos')->with('successed', 'La marca se eliminó exitasamente!');
	}
}
