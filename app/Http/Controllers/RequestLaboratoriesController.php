<?php

namespace App\Http\Controllers;

use Auth;
use Calendar;
use App\Subject;
use App\Category;
use App\RequestLaboratory;
use Illuminate\Http\Request;
use App\RequestLaboratoriesDetail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class RequestLaboratoriesController extends Controller
{
    // Método constructor
    // Middleware para la autenticación del usuario
    public function __construct(){
        $this->middleware('auth');
    }
    // Método que muestra la´página de incio
    public function index(){
        // Consulta que obtiene todas las solicitudes aceptadas
        $details = DB::table('request_laboratories AS laboratory')
            ->leftJoin('users', 'laboratory.user_id', '=', 'users.id')
            ->leftJoin('request_laboratories_details AS detail', 'laboratory.id', '=', 'detail.request_laboratory_id')
            ->leftJoin('categories', 'detail.category_id', '=', 'categories.id')
            ->leftJoin('subjects', 'detail.subject_id', '=', 'subjects.id')
            ->leftJoin('statuses', 'detail.status_id', '=', 'statuses.id')
            ->select('detail.*', 'users.name as user', 'categories.name AS category', 'subjects.name AS subject', 'statuses.id AS status_id', 'statuses.name AS status')
            ->get();
    	
        if(Auth::user()->id == 3 || Auth::user()->id == 4){
            
            return view('solicitudes.index', compact('details'));
        }
        elseif(Auth::user()->id == 2){
            //Fecha actual
            $currentDate = strtotime(date('d-m-Y',time()));

            return view('solicitudes.indexalmacen', compact('currentDate','details'));
        }
    }

    // Función para la vista de solicitud de espacios
    public function create(){
        if(Auth::user()->id == 3 || Auth::user()->id == 4){
            //Fecha actual
            $currentDate = strtotime(date('d-m-Y',time()));
            // Se obtiene los datos del usuario autenticado
            $user = Auth::user()->id;
            // Se genera la consulta que obtiene las solicitudes realizadas por dicho usuario
            $details = DB::table('request_laboratories AS laboratory')
                ->leftJoin('request_laboratories_details AS detail', 'laboratory.id', '=', 'detail.request_laboratory_id')
                ->leftJoin('categories', 'detail.category_id', '=', 'categories.id')
                ->leftJoin('subjects', 'detail.subject_id', '=', 'subjects.id')
                ->leftJoin('statuses', 'detail.status_id', '=', 'statuses.id')
                ->select('detail.*', 'categories.name AS category', 'subjects.name AS subject', 'statuses.id AS status_id', 'statuses.name AS status')
                ->where('user_id', '=', $user)
                ->get();
            // Se obtienen las asignaturas disponibles
            $subjects = Subject::all();
            //Se obtienen los laboratorios disponibles
            $laboratories = Category::all();
            // Se retornan los datos del usuario, los detalles de la solicitud, las asignaturas y laboratorios
            // Se manda a llamar a la vista solicitudes/crear con los datos empaquetos
            return view('solicitudes.crear', compact('currentDate','user', 'details', 'subjects', 'laboratories'));
        }
    }
    // Función encargada de insertar la solicitud del espacio
    public function addEvent(Request $request){
        //Fecha actual
        $currentDate = strtotime(date('d-m-Y',time()));
        // Fecha de solicitud formateada
        $date = strtotime($request->fecha);
        // Identifica si es fin de semana
        $isWeekend = date('N', strtotime($request->fecha)) >= 6;
        //Se obtienen los minutos de las horas
        $startMinutes = explode(':', $request->inicio);
        $endMinutes = explode(':', $request->inicio);
        // Convierte las horas de string a int
        $startTime = strtotime($request->inicio);
        $endTime = strtotime($request->fin);
        // Se valida que los campos requeridos del formulario sean llenados
        $request->validate([
            'subject_id'=>'required',
            'category_id'=>'required',
            'fecha'=>'required',
            'inicio'=>'required',
            'fin'=>'required'
        ]);
        // Se valida si es fin de semana o no
        if($isWeekend == true){
            return redirect('/solicitudes/crear')->with('warning', 'La fecha de solicitud debe ser entre lunes y viernes!');
        // Se valida si la fecha de inicio es mayor a la fecha fin
        }elseif($date < $currentDate){
            return redirect('/solicitudes/crear')->with('warning', 'La fecha de solicitud debe ser mayor o igual a la fecha actual!');
        }elseif($startTime > $endTime || $startTime == $endTime){
            return redirect('/solicitudes/crear')->with('warning', 'La hora de inicio no puede ser mayor o igual a la hora fin!');
        }elseif($startMinutes[1] > 00 || $endMinutes[1] > 00){
            return redirect('/solicitudes/crear')->with('warning', 'Lo sentimos, no se aceptan minutos...');
        }
        else{
            $requestDetails = RequestLaboratoriesDetail::where([
                ['subject_id', '=', $request->subject_id], 
                ['category_id', '=', $request->category_id],
                ['date', '=', $request->fecha], 
                ['starts', '=', $request->inicio], 
                ['ends', '=', $request->fin], 
                ['status_id', '=', '6'],
            ])->get();
            // Valida si existe una solicitud registrada
            if(count($requestDetails) > 0){
                return redirect('/solicitudes/crear')->with('warning', 'Ups! Parece que ya tenemos reservado ese lugar...');
            }else{
                // Se obtiene el id del usuario del request
                $usuario = $request->get('user_id');
                // Se verifica si no existe un registro con ese usuario en la tabla request_laboratories
                $solicitudLaboratorio = DB::table('request_laboratories')->where('user_id', $usuario)->doesntExist();
                // Si no existe el usuario, este es creado
                if($solicitudLaboratorio == true){
                    $requestLaboratories = new RequestLaboratory([
                        'user_id' => $usuario,
                        'status_id' => 1
                    ]);
                    $requestLaboratories->save();
                }
                // Se obtiene el id de la solicitud del laboratorio donde user_id se igual al usuario autenticado
                $laboratoryID = DB::table('request_laboratories')
                    ->select('id')
                    ->where('user_id', '=', $usuario)
                    ->first();
                // Se crea una nueva instancia del objeto RequestLaboratoriesDetail
                // Se le concatenan los datos obtenidos del forumlario
                $requestLaboratoriesDetail = new RequestLaboratoriesDetail([
                    'request_laboratory_id' => $laboratoryID->id,
                    'subject_id' => $request->get('subject_id'),
                    'category_id' => $request->get('category_id'),
                    'date' => $request->get('fecha'),
                    'starts'=> $request->get('inicio'),
                    'ends' => $request->get('fin'),
                    'status_id' => 5
                ]);
                // Se guardan los datos del objeto RequestLaboratoriesDetail creado
                $requestLaboratoriesDetail->save();
                //Se retorna a la vista de solicitud de materiales
                return redirect('/solicitudes/crear')->with('successed', 'La solicitud de laboratorio se registró exitasamente!');
            }
        }
    }

    public function acceptRequest($id){
        $details = RequestLaboratoriesDetail::find($id);

        $details->status_id = 6;

        $details->save();
        
        return redirect('/solicitudes')
        ->with('successed', 'Solicitud aprobada!');
    }

    public function destroy($id){
        if(Auth::user()->id == 3 || Auth::user()->id == 4){
            $detail = RequestLaboratoriesDetail::find($id);
            $detail->delete();

            return redirect('/solicitudes/crear')->with('successed', 'La solicitud de laboratorio se eliminó exitasamente!');   
        }elseif(Auth::user()->id == 2){
            $detail = RequestLaboratoriesDetail::find($id);
            $detail->delete();

            return redirect('/solicitudes')->with('successed', 'Solicitud rechazada!');
        }
    }
}
