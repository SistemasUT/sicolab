<?php

namespace App\Http\Controllers;

use App\Unit;
use App\Type;
use App\Brand;
use App\Material;
use App\Category;

use Illuminate\Http\Request;

class MaterialController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$materials = Material::orderBy('id', 'asc')->latest()->paginate(7);

        return view('materiales.index', compact('materials'))->with('i', (request()->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = Type::all();
        $units = Unit::all();
        $categories = Category::all();
        $brands = Brand::all();

        return view('materiales.crear', compact('types', 'units','categories','brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'model'=>'required|max:100',
            'description'=> 'max:300',
            'type_id'=>'required',
            'unit_id'=>'required',
            'category_id'=>'required',
            'brand_id'=>'required'
        ]);
        // Se obtiene el tiempo y se le concatena la extensión de la imagen
        $img = time().'.'.$request->image->getClientOriginalExtension();

        $material = new Material([
            'image' => $img,
            'model' => $request->get('model'),
            'description'=> $request->get('description'),
            'type_id' => $request->get('type_id'),
            'unit_id' => $request->get('unit_id'),
            'category_id' => $request->get('category_id'),
            'brand_id' => $request->get('brand_id')
        ]);
        
        // Se guarda la imagen en la ruta pública /img
        $request->image->move(public_path('img'), $img);

        $material->save();
        
        return redirect('/materiales')
        ->with('successed', 'El material se registró exitasamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $material = Material::find($id);

        return view('materiales.show',compact('material'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $material = Material::find($id);
        $types = Type::all();
        $units = Unit::all();
        $categories = Category::all();
        $brands = Brand::all();

        return view('materiales.editar', compact('types', 'material','units','categories','brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {  
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'model'=>'required|max:100',
            'description'=> 'max:300',
            'type_id'=>'required',
            'unit_id'=>'required',
            'category_id'=>'required',
            'brand_id'=>'required'
        ]);
        
        $material = Material::find($id);

        if($request->hasFile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $fileName =  time().'.'.$extension;
            $file->move(public_path('img'), $fileName);
            $material->image = $fileName;
        }

        $material->model = $request->get('model');
        $material->description = $request->get('description');
        $material->type_id = $request->get('type_id');
        $material->unit_id = $request->get('unit_id');
        $material->category_id = $request->get('category_id');
        $material->brand_id = $request->get('brand_id');

        $material->save();

        return redirect('/materiales')->with('successed', 'El material se modificó exitasamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $material = Material::find($id);
        $material->delete();

        return redirect('/materiales')->with('successed', 'El material se eliminó exitasamente!');
    }
}
