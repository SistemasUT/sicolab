<?php

namespace App\Http\Controllers;

use Auth;
use App\Unit;
use App\Area;
use App\Stock;
use App\Material;
use Carbon\Carbon;
use App\Departure;
use App\Requisition;
use App\RequisitionDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Dompdf\Dompdf;

class RequisitionController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        if(Auth::user()->id == 1){
            $requisitions = Requisition::all();
            
            return view('requisiciones.index', compact('requisitions'));
        }elseif(Auth::user()->id == 2){
            $requisitions = Requisition::where('status_id', 2)->orWhere('status_id', 4)->get();
            
            return view('requisiciones.indexalmacen', compact('requisitions'));
        }
    }

    public function create(){

        $areas = Area::all();

        return view('requisiciones.crear', compact('areas'));
    }

    public function assign($id){
        if(Auth::user()->id == 1){
            $stocks = DB::table('stocks')
                ->leftJoin('materials', 'stocks.material_id', '=', 'materials.id')
                ->leftJoin('types', 'materials.type_id', '=', 'types.id')
                ->leftJoin('units', 'materials.unit_id', '=', 'units.id')
                ->select('stocks.material_id as id', 'types.name', 'units.name as unit', DB::raw('COUNT(stocks.material_id) AS amount'))
                ->groupBy('stocks.material_id')
                ->having('amount', '>=', 1)
                ->get();

            $departures = Departure::all();
            $units = Unit::all();
            $materials = Material::all();
            $requisition = Requisition::find($id);
            $requisitionID = $id;
            $requisitionsDetails = DB::table('requisition_details')
                ->leftJoin('departures', 'requisition_details.departure_id', '=', 'departures.id')
                ->leftJoin('materials', 'requisition_details.material_id', '=', 'materials.id')
                ->leftJoin('types', 'materials.type_id', '=', 'types.id')
                ->leftJoin('units', 'materials.unit_id', '=', 'units.id')
                ->select('requisition_details.*', 'departures.name as departure', 'types.name as material', 'units.name as unit')
                ->where('requisition_details.requisition_id', '=', $id)
                ->get();

            return view('requisiciones.asignar',compact('requisitionID', 'stocks','departures', 'units', 'materials', 'requisition','requisitionsDetails'));
        }elseif(Auth::user()->id == 2) {
            $requisition = Requisition::find($id);
            $requisitionID = $id;
            $requisitionsDetails = DB::table('requisition_details')
                ->leftJoin('statuses', 'requisition_details.status_id', '=', 'statuses.id')
                ->leftJoin('departures', 'requisition_details.departure_id', '=', 'departures.id')
                ->leftJoin('materials', 'requisition_details.material_id', '=', 'materials.id')
                ->leftJoin('types', 'materials.type_id', '=', 'types.id')
                ->leftJoin('units', 'materials.unit_id', '=', 'units.id')
                ->select('requisition_details.*', 'departures.name as departure', 'types.name as material', 'units.name as unit', 'statuses.name as status')
                ->where([['requisition_details.requisition_id', '=', $id], ['requisition_details.status_id', '=', '3'],])
                ->orWhere([['requisition_details.requisition_id', '=', $id], ['requisition_details.status_id', '=', '9'],])
                ->get();

            return view('requisiciones.almacenaralmacen',compact('requisitionID', 'requisition','requisitionsDetails'));
        }
    }

    public function details($idRequisition,$idDetail){

        //$material_id = $_GET['material'];

        $materials = Material::all();

        $requisitionsDetails = DB::table('requisition_details')
            ->leftJoin('materials', 'requisition_details.material_id', '=', 'materials.id')
            ->leftJoin('types', 'materials.type_id', '=', 'types.id')
            ->select('requisition_details.*', 'types.name as material')
            ->where('requisition_details.id', '=', $idDetail)
            ->first();

        //$stockItems = Stock::where([['requisition_id', '=', $idRequisition], ['material_id', '=', $material_id],])->get();
        $stockItems = Stock::where('requisition_id', '=', $idRequisition)->get();

        return view('requisiciones.detalles', compact('materials', 'requisitionsDetails', 'idRequisition', 'idDetail', 'stockItems'));
    }

    public function downloadQR($idRequisition){
        $stockItems = Stock::where('requisition_id', '=', $idRequisition)->get();

        //return view('requisiciones.qrs', compact('stockItems'));

        $invoice = "2222";
        $view =  \View::make('requisiciones.qrs', compact('stockItems'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper('letter', 'landscape');

        return $pdf->stream('invoice');
    }

    public function receive($id){

        $requisition = Requisition::find($id);
        $requisitionID = $id;
        $requisitionsDetails = DB::table('requisition_details')
            ->leftJoin('statuses', 'requisition_details.status_id', '=', 'statuses.id')
            ->leftJoin('departures', 'requisition_details.departure_id', '=', 'departures.id')
            ->leftJoin('materials', 'requisition_details.material_id', '=', 'materials.id')
            ->leftJoin('types', 'materials.type_id', '=', 'types.id')
            ->leftJoin('units', 'materials.unit_id', '=', 'units.id')
            ->select('requisition_details.*', 'departures.name as departure', 'types.name as material', 'units.name as unit', 'statuses.name as status')
            ->where('requisition_details.requisition_id', '=', $id)
            /*->WhereNull('requisition_details.status_id')*/
            ->get();

        return view('requisiciones.recepcionar',compact('requisition', 'requisitionID', 'requisitionsDetails'));
    }

    public function sendRequisition($id){

        $requisition = Requisition::find($id);

        if($requisition->status_id == 2) {
            $requisition->status_id = 4;
        }else{
            $requisition->status_id = 2;
        }

        $requisition->save();
        
        return redirect('/requisiciones')
        ->with('successed', 'La requisición se envió exitasamente!');
    }

    public function excel($id){

        $requisition = Requisition::find($id);
        $requisitionsDetails = DB::table('requisition_details')
            ->leftJoin('departures', 'requisition_details.departure_id', '=', 'departures.id')
            ->leftJoin('materials', 'requisition_details.material_id', '=', 'materials.id')
            ->leftJoin('types', 'materials.type_id', '=', 'types.id')
            ->leftJoin('units', 'materials.unit_id', '=', 'units.id')
            ->select('requisition_details.*', 'departures.name as departure', 'types.name as material', 'units.name as unit')
            ->where('requisition_details.requisition_id', '=', $id)
            ->get();

        return view('requisiciones.imprimir',compact('requisition','requisitionsDetails'));
    }

    public function store(Request $request){
        
        $request->validate([
            'concepto'=>'required|max:200',
            'evento'=>'required|max:200',
            'folio'=>'required|max:200',
            'area_id'=>'required'
        ]);

        $requisition = new Requisition([
            'concept' => $request->get('concepto'),
            'event' => $request->get('evento'),
            'folio' => $request->get('folio'),
            'area_id'=> $request->get('area_id'),
            'user_id' => Auth::user()->id,
            'status_id' => 1
        ]);

        $requisition->save();
        
        return redirect('/requisiciones')
        ->with('successed', 'La requisición se registró exitasamente!');
    }

    public function storeDetails(Request $request){

        if(Auth::user()->id == 1){

            $requisition_id = $request->requisition_id;

            DB::table('requisition_details')->where('requisition_id', '=', $requisition_id)->delete();

            $component = $request->component;
            $departure_id = $request->departure_id;
            $amount = $request->amount;
            $stock_id = $request->stock_id;

            $count = count($departure_id);

            for($i = 0; $i < $count; $i++){
                $requisitionDetail = new RequisitionDetail();
                $requisitionDetail->component = $component[$i];
                $requisitionDetail->amount = $amount[$i];
                $requisitionDetail->departure_id = $departure_id[$i];
                $requisitionDetail->requisition_id = $requisition_id;
                $requisitionDetail->material_id = $stock_id[$i];
                $requisitionDetail->save();
            }

            return redirect('/requisiciones/'.$requisition_id)->with('successed', 'Materiales modificados exitosamente!');

        }elseif(Auth::user()->id == 2){

            $requisition_id = $request->requisition_id;
            $id = $request->requisitionDetail;
            $storage = $request->storage;
            $count = count($id);

            for ($i=0; $i < $count; $i++) {

                $requisitionDetail = RequisitionDetail::find($id[$i]);

                $requisitionDetail->storage = $storage[$i];

                $requisitionDetail->save();
            }

            return redirect('/requisiciones/'.$requisition_id)->with('successed', 'La cantidad recibida modificada exitosamente!');
        }
    }

    public function pullItemToStock(Request $request){

        $request->validate([
            'serial.*'=>'required|string|max:100'
        ]);

        $serial = $request->serial;
        $expiration = $request->expiration;
        $requisition_id = $request->requisition_id;
        $material_id = $request->material_id;
        $itemDetailID = $request->detail_id;

        $count = count($serial);

        for($i = 0; $i < $count; $i++){
            $stockDetails = new Stock();
            $stockDetails->serial = $serial[$i];
            $stockDetails->expiration = $expiration[$i];
            $stockDetails->requisition_id = $requisition_id;
            $stockDetails->material_id = $material_id[$i];

            $stockDetails->save();
        }

        $requisitionItem = RequisitionDetail::find($itemDetailID);

        $requisitionItem->status_id = 9;
        $requisitionItem->save();

        return redirect('/requisiciones/'.$requisition_id)->with('successed', 'Materiales modificados exitosamente!');
    }

    public function storeDetailsReceive(Request $request){

        $requisition_id = $request->requisition_id;
        $id = $request->id;
        $component = $request->component;
        $departure_id = $request->departure_id;
        $amount = $request->amount;
        $receive = $request->receive;
        $material_id = $request->material_id;
        $description = $request->description;
        $status_id = $request->status_id;
        $count = count($departure_id);
        $ids = array();
        $statuses = array();

        if(!empty($status_id)) {
            foreach($status_id as $key => $status){
                $idStatus = explode("_", $status);

                array_push($ids, $idStatus[0]);
                array_push($statuses, $idStatus[1]);
            }
        }
        
        for($i = 0; $i < $count; $i++){
            $requisitionDetail = RequisitionDetail::find($id[$i]);

            $requisitionDetail->receive = $receive[$i];
            $requisitionDetail->description = $description[$i];

            if(empty($status_id)){
                $requisitionDetail->status_id = NULL;
            }elseif(in_array($id[$i], $ids)){
                $index = array_search($id[$i], $ids);
                $requisitionDetail->status_id = $statuses[$index];
            }else{
                $requisitionDetail->status_id = NULL;
            }

            $requisitionDetail->save();
        }

        return redirect('/requisiciones/recepcionar/'.$requisition_id)->with('successed', 'Materiales modificados exitosamente!');
    }

    public function destroy($id)
    {

        $requisitionDetail = RequisitionDetail::where('requisition_id', $id);
        $requisitionDetail->delete();

        $requisition = Requisition::find($id);
        $requisition->delete();


        return redirect('/requisiciones')->with('successed', 'La requisición se eliminó exitasamente!');
    }
}