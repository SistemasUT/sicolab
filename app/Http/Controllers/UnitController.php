<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unit;

class UnitController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$units = Unit::all();
    	$units = Unit::orderBy('id', 'asc')->latest()->paginate(7);

        return view('unidades.index', compact('units'))->with('i', (request()->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('unidades.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'name'=>'required|max:80',
        'description'=> 'max:300'
      ]);
      $unit = new Unit([
        'name' => $request->get('name'),
        'description'=> $request->get('description')
      ]);
      $unit->save();
      return redirect('/unidades')
      	->with('successed', 'La unidad se registró exitasamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$unit = Unit::find($id);

        return view('unidades.show',compact('unit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unit = Unit::find($id);

        return view('unidades.editar', compact('unit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$request->validate([
        	'name'=>'required|max:80',
        	'description'=> 'max:300'
      	]);

      	$unit = Unit::find($id);
    	$unit->name = $request->get('name');
    	$unit->description = $request->get('description');
      	$unit->save();

      	return redirect('/unidades')
      		->with('successed', 'La unidad se modificó exitasamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$unit = Unit::find($id);
    	$unit->delete();

    	return redirect('/unidades')->with('successed', 'La unidad se eliminó exitasamente!');
	}
}
