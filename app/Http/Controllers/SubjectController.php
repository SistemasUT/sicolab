<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;

class SubjectController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$areas = Area::all();
    	$asignaturas = Subject::orderBy('id', 'asc')->latest()->paginate(7);

        return view('asignaturas.index', compact('asignaturas'))->with('i', (request()->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('asignaturas.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'name'=>'required|max:80',
        'description'=> 'max:300'
      ]);
      $asignatura = new Subject([
        'name' => $request->get('name'),
        'description'=> $request->get('description')
      ]);
      $asignatura->save();
      return redirect('/asignaturas')
      	->with('successed', 'La asignatura se registró exitasamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$asignatura = Subject::find($id);

        return view('asignaturas.show',compact('asignatura'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asignatura = Subject::find($id);

        return view('asignaturas.editar', compact('asignatura'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$request->validate([
        	'name'=>'required|max:80',
        	'description'=> 'max:300'
      	]);

      	$asignatura = Subject::find($id);
    	$asignatura->name = $request->get('name');
    	$asignatura->description = $request->get('description');
      	$asignatura->save();

      	return redirect('/asignaturas')
      		->with('successed', 'La asignatura se modificó exitasamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$asignatura = Subject::find($id);
    	$asignatura->delete();

    	return redirect('/asignaturas')->with('successed', 'La asignatura se eliminó exitasamente!');
	}
}
