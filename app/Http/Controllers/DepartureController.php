<?php

namespace App\Http\Controllers;

use App\Departure;
use Illuminate\Http\Request;

class DepartureController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$departures = Departure::all();
    	$departures = Departure::orderBy('id', 'asc')->latest()->paginate(7);

        return view('partidas.index', compact('departures'))->with('i', (request()->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('partidas.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'name'=>'required|max:80',
        'description'=> 'max:300'
      ]);
      $departure = new Departure([
        'name' => $request->get('name'),
        'description'=> $request->get('description')
      ]);
      $departure->save();
      return redirect('/partidas')
      	->with('successed', 'La marca se registró exitasamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$departure = Departure::find($id);

        return view('partidas.show',compact('departure'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departure = Departure::find($id);

        return view('partidas.editar', compact('departure'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$request->validate([
        	'name'=>'required|max:80',
        	'description'=> 'max:300'
      	]);

      	$departure = Departure::find($id);
    	$departure->name = $request->get('name');
    	$departure->description = $request->get('description');
      	$departure->save();

      	return redirect('/partidas')
      		->with('successed', 'La marca se modificó exitasamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$departure = Departure::find($id);
    	$departure->delete();

    	return redirect('/partidas')->with('successed', 'La marca se eliminó exitasamente!');
	}
}
