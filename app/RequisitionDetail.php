<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequisitionDetail extends Model
{
    protected $fillable =[
	    'component', 
        'amount',
        'receive',
        'departure_id',
        'requisition_id',
        'material_id',
        'status_id'
    ];

    public function requisition()
    {
        return $this->hasMany('App\Requisition');
    }
    public function material()
    {
        return $this->belongsTo('App\Material');
    }
}
