<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requisition extends Model
{
    protected $fillable =[
	    'concept', 
        'event',
        'folio',
        'area_id',
        'user_id',
        'status_id'
    ];

    public function requisitionDetail()
    {
        return $this->belongsTo('App\requisitionDetail');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function area()
    {
        return $this->belongsTo('App\Area');
    }

    public function departure()
    {
        return $this->belongsTo('App\Departure');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }
}
