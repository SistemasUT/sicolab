@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Editar Tipo de Material</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form method="post" action="{{ route('tipos.update', $type->id) }}">
      @method('PATCH')
      @csrf
      <div class="form-group">
        <label for="name">Tipo de material:</label>
        <input type="text" class="form-control" name="name" value="{{ $type->name }}" />
      </div>
      <div class="form-group">
        <label for="description">Descripción:</label>
        <textarea class="form-control" rows="5" name="description">{{ $type->description }}</textarea>
      </div>
      <a class="btn btn-default" href="{{ route('tipos.index') }}"> Cancelar</a>
      <button type="submit" class="btn btn-primary">Actualizar</button>
    </form>
  </div>
</div>

@endsection