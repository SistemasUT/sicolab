@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Catálogo de Tipos de Materiales</h3>
    <div class="box-tools pull-right">
      <a class="btn btn-success fa fa-plus" href="{{ route('tipos.create') }}"> Agregar</a>
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table class="table table-striped">
      <thead>
          <tr>
            <td>ID</td>
            <td>Tipo del material</td>
            <td>Descripción</td>
            <td>Acciones</td>
          </tr>
      </thead>
      <tbody>
        @foreach($types as $type)
          <tr>
            <td>{{$type->id}}</td>
            <td>{{$type->name}}</td>
            <td>{{$type->description}}</td>
            <td>
              <form action="{{ route('tipos.destroy', $type->id) }}" method="POST">
                <a class="btn btn-info fa fa-eye" href="{{ route('tipos.show',$type->id) }}"></a>
                <a class="btn btn-primary fa fa-pencil" href="{{ route('tipos.edit',$type->id) }}"></a>
                @csrf
                @method('DELETE')
                <button class="btn btn-danger fa fa-trash" type="submit"></button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    {!! $types->links() !!}
  </div>
</div>

@endsection