@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Ver Tipo de Material</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="form-group">
      <strong>Tipo de material:</strong>
      {{ $type->name }}
    </div>
    <div class="form-group">
      <strong>Descripción:</strong>
      {{ $type->description }}
    </div>
    <a class="btn btn-default" href="{{ route('tipos.index') }}"> Cerrar</a>
  </div>
</div>

@endsection