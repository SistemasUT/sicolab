@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Préstamo de materiales</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nombre del material</th>
            <th>Cantidad</th>
            <th>Unidad</th>
          </tr>
        </thead>
        <tbody class="find">
          @foreach($details as $detail)
          <tr>
            <td>{{$detail->id}}</td>
            <td>{{$detail->material}}</td>
            <td>{{$detail->amount}}</td>
            <td>{{$detail->unit}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
      <a class="btn btn-default" href="{{ route('prestamos.index') }}"> Cerrar</a>
  </div>
</div>

@endsection