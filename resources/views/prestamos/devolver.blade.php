@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Devolución de material</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <!--Formulario de la requisición-->
    <form method="post" action="{{ route('prestados.devolver.guardar') }}" enctype="multipart/form-data">
      @csrf
      <input type="hidden" name="id" value="{{ $id }}">
      <table id="loanTable" class="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nombre del material</th>
            <th>Cant. prestada</th>
            <th>Unidad</th>
            {{--<th>Acciones</th>--}}
          </tr>
        </thead>
        <tbody class="find">
          @foreach($details as $detail)
          <tr  id="{{$detail->id}}">
            <td><input type="hidden" name="details_id[]" value="{{$detail->id}}"/>{{$detail->id}}</td>
            <td><input type="hidden" name="stock_id[]" value="{{$detail->material_id}}"/>{{$detail->material}}</td>
            <td><input type="hidden" name="given[]" value="{{$detail->amount}}"/>{{$detail->amount}}</td>
            <td><input type="hidden" name="status_id[]" value="{{ $detail->id."_"."7" }}"/>{{$detail->unit}}</td>
            {{--<td>
              <label class="switch">
                <input name="status_id[]" value="{{ $detail->id."_"."8" }}" type="checkbox">
                <span class="slider round"></span>
              </label>
            </td>--}}
          </tr>
          @endforeach
        </tbody>
      </table>
      <a class="btn btn-default" href="{{ route('prestamos.index') }}">Regresar</a>
      <button type="submit" class="btn btn-primary">Devolver</button>
      {{--<a class="btn btn-primary " href="{{ route('prestamos.enviar', $id) }}">Devolver</a>--}}
    </form>
  </div>
</div>

@endsection