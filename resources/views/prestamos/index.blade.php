@extends('dashboard')

@section('content')
<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Solicitud de préstamo de material</h3>
    <div class="box-tools pull-right">
      <a class="btn btn-seccess fa fa-plus" href="{{ route('prestamos.crear') }}"> Agregar</a>
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table class="table table-striped">
      <thead>
          <tr>
            <td>ID</td>
            {{--<td>Área</td>--}}
            <td>Nombre del evento</td>
            <td>Descripción</td>
            {{--<td>Evento</td>--}}
            <td>Fecha</td>
            <td>Status</td>
            <td>Acciones</td>
          </tr>
      </thead>
      <tbody>
        @foreach($loans as $loan)
          <tr>
            <td>{{$loan->id}}</td>
            {{--<td>{{$loan->area->name}}</td>--}}
            <td>{{$loan->event}}</td>
            <td>{{$loan->description}}</td>
            {{--<td>{{$loan->event}}</td>--}}
            <td>
              {{'Del '.Carbon\Carbon::parse($loan->starts)->format('d-m-Y')}}
              {{' al '.Carbon\Carbon::parse($loan->ends)->format('d-m-Y')}}
              {{' de '.Carbon\Carbon::parse($loan->starts)->format('H:i')}}
              {{' a '.Carbon\Carbon::parse($loan->ends)->format('H:i')}}
            </td>
            <td>
              @if($loan->status->id == 5)
                <span class="label label-info">{{$loan->status->name}}</span>
              @elseif($loan->status->id == 2)
                <span class="label label-primary">{{$loan->status->name}}</span>
              @elseif($loan->status->id == 6)
                <span class="label label-success">{{$loan->status->name}}</span>
              @elseif($loan->status->id == 7)
                <span class="label label-primary">{{$loan->status->name}}</span>
              @elseif($loan->status->id == 8)
                <span class="label label-primary">{{$loan->status->name}}</span>
              @elseif($loan->status->id == 4)
                <span class="label label-danger">{{$loan->status->name}}</span>
              @endif
            </td>
            <td>
              <form action="{{ route('prestamos.eliminar', $loan->id) }}" method="POST">
                @if($loan->status->id == 5)
                  <a class="btn btn-primary fa fa-send" href="{{ route('prestamos.enviar',$loan->id) }}"></a>
                  <a class="btn btn-success fa fa-cart-plus" href="{{ route('prestamos.asignar',$loan->id) }}"></a>
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger fa fa-trash" type="submit"></button>
                @elseif($loan->status->id == 2)
                  <a class="btn btn-primary fa fa-eye" href="{{ route('prestamos.ver',$loan->id) }}"></a>
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger fa fa-trash" type="submit"></button>
                @elseif($loan->status->id == 6)
                  <a class="btn btn-primary fa fa-reply" href="{{ route('prestamos.devolver',$loan->id) }}"></a>
                @elseif($loan->status->id == 7)
                  <a class="btn btn-primary fa fa-eye" href="{{ route('prestamos.ver',$loan->id) }}"></a>
                @elseif($loan->status->id == 8)
                  <a class="btn btn-primary fa fa-eye" href="{{ route('prestamos.ver',$loan->id) }}"></a>
                @elseif($loan->status->id == 4)
                  <a class="btn btn-primary fa fa-eye" href="{{ route('prestamos.ver',$loan->id) }}"></a>
                @endif
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>

@endsection