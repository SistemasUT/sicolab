@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Préstamo de materiales</h3>
    <div class="box-tools pull-right">
      <a class="btn btn-success fa fa-search" data-toggle="modal" data-target=".bd-example-modal-lg"> Buscar</a>
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <!--Formulario de la requisición-->
    <form method="post" action="{{ route('prestamos.detalles.guardar') }}" enctype="multipart/form-data">
      @csrf
      <input type="hidden" name="loan_id" value="{{ $loanID }}">
      <table id="loanTable" class="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nombre del material</th>
            <th>Cantidad</th>
            <th>Unidad</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach($details as $detail)
          <tr  id="{{$detail->id}}">
            <td>{{$detail->id}}</td>
            <td><input type="hidden" name="stock_id[]" value="{{$detail->material_id}}"/>{{$detail->material}}</td>
            <td><input type="number" class="form-control" name="amount[]" min="1" max="{{$detail->loan}}" value="{{$detail->loan}}"/></td>
            <td>{{$detail->unit}}</td>
            <td>
              <button class="btn btn-danger fa fa-trash" type="button" onclick="deleteRow({{$detail->id}})"></button>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      <a class="btn btn-default" href="{{ route('prestamos.index') }}">Regresar</a>
      <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
  </div>
</div>
<!-- Busqueda de materiales -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Busqueda de materiales</h3>
      </div>
      <div class="modal-body">
        <div class="input-group">
          <span class="input-group-addon">Buscar</span>
          <input id="finder" type="text" class="form-control" placeholder="Ingrese el nombre del material a buscar...">
        </div>
        <br>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Nombre Del material</th>
              <th>Cantidad</th>
              <th>Unidad</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody class="find">
            @foreach($stocks as $stock)
              @if($stock->stock == $stock->loan)
              @elseif($stock->available != null)
                <tr  id="{{$stock->id}}">
                  <td>{{$stock->id}}</td>
                  <td>{{$stock->name}}</td>
                  <td>{{$stock->available}}</td>
                  <td>{{$stock->unit}}</td>
                  <td>
                    <a class="btn btn-primary fa fa-plus" onclick="addMaterial({{$stock->id}})"></a>
                  </td>
                </tr> 
              @else
                <tr  id="{{$stock->id}}">
                  <td>{{$stock->id}}</td>
                  <td>{{$stock->name}}</td>
                  <td>{{$stock->stock}}</td>
                  <td>{{$stock->unit}}</td>
                  <td>
                    <a class="btn btn-primary fa fa-plus" onclick="addMaterial({{$stock->id}})"></a>
                  </td>
                </tr>
              @endif
            @endforeach
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>
<script>
  $(document).ready(function(){
    (function($){
      $('#finder').keyup(function(){
        var rex = new RegExp($(this).val(), 'i');
        $('.find tr').hide();
        $('.find tr').filter(function(){
            return rex.test($(this).text());
        }).show();
      })
    }(jQuery));
  });

  function addMaterial(id){
    var name = document.getElementById(id).cells[1].innerHTML;
    var amount = document.getElementById(id).cells[2].innerHTML;
    var unit = document.getElementById(id).cells[3].innerHTML;
    var stockID = document.getElementById(id).cells[0].innerHTML;
   
    var newRow="<tr>";
        newRow+="<td></td>";
        newRow+="<td>"+name+"<input type='hidden' name='stock_id[]' value='"+stockID+"'/></td>";
        newRow+="<td><input type='number' class='form-control' name='amount[]' min='1' max='"+amount+"' value='"+amount+"'/></td>";
        newRow+="<td>"+unit+"</td>";
        newRow+="<td><button class='btn btn-danger fa fa-trash' type='button' onclick='deleteRow()'></button></td>";
        newRow+="</tr>";
    $("#loanTable").append(newRow);
  }

  function deleteRow(rowid){ 
    var row = document.getElementById(rowid);
    row.parentNode.removeChild(row);
  }
</script>

@endsection