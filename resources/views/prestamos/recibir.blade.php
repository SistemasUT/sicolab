@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Devolución de material</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <!--Formulario de la requisición-->
    <form method="post" action="{{ route('prestados.almacenar') }}" enctype="multipart/form-data">
      @csrf
      <input type="hidden" name="loan_id" value="{{ $id }}">
      <table id="loanTable" class="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nombre del material</th>
            <th>Cant. devuelta</th>
            <th>Cant. recibida</th>
            <th>Unidad</th>
          </tr>
        </thead>
        <tbody class="find">
          @foreach($details as $detail)
          <tr  id="{{$detail->id}}">
            <td><input type="hidden" name="details_id[]" value="{{$detail->id}}"/>{{$detail->id}}</td>
            <td><input type="hidden" name="stock_id[]" value="{{$detail->material_id}}"/>{{$detail->material}}</td>
            <td><input type="hidden" name="amount[]" value="{{$detail->amount}}"/>{{$detail->amount}}</td>
            <td><input type="number" class="form-control" name="returned[]" min="1" max="{{$detail->amount}}" value="{{$detail->amount}}"/></td>
            <td>{{$detail->unit}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
      <a class="btn btn-default" href="{{ route('prestamos.index') }}">Regresar</a>
      <button type="submit" class="btn btn-primary">Almacenar</button>
      {{--<a class="btn btn-primary " href="{{ route('prestamos.enviar', $id) }}">Recibir</a>--}}
    </form>
  </div>
</div>

@endsection