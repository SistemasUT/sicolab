@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Préstamo de materiales</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form method="post" action="{{ route('prestados.guardar') }}" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="loan_id" value="{{ $id }}">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre del material</th>
          <th>Cant. solicitada</th>
          <th>Cant. prestada</th>
          <th>Unidad</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody class="find">
        @foreach($details as $detail)
        <tr>
          <td>{{$detail->id}}<input type="hidden" name="id[]" value="{{$detail->id}}"></td>
          <td>{{$detail->material}}</td>
          <td>{{$detail->amount}}</td>
          <td>
            @if($detail->status_id == null)
              @if($detail->given == null)
                <input type="number" class="form-control" name="given[]" min="1" max="{{$detail->amount}}" value="{{$detail->amount}}"/>
              @else
                <input type="number" class="form-control" name="given[]" min="1" max="{{$detail->amount}}" value="{{$detail->given}}"/>
              @endif
            @else
              {{$detail->given}}<input type="hidden" name="given[]" value="{{$detail->given}}">
            @endif
          </td>
          <td>{{$detail->unit}}</td>
          <td>
            @if($detail->status_id == 10)
              <span class="label label-primary">{{$detail->status}}</span>
              <input type="hidden" name="status_id[]" value="{{ $detail->id."_".$detail->status_id }}"/>
            @else
              <label class="switch">
                <input name="status_id[]" value="{{ $detail->id."_"."10" }}" type="checkbox">
                <span class="slider round"></span>
              </label>
            @endif
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <a class="btn btn-default" href="{{ route('prestamos.index') }}"> Cerrar</a>
    <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
  </div>
</div>

@endsection