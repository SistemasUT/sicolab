@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Materiales prestados</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre del material</th>
          <th>Cant. solicitada</th>
          <th>Cant. prestada</th>
          <th>Unidad</th>
          <th>Estado</th>
        </tr>
      </thead>
      <tbody class="find">
        @foreach($details as $detail)
        <tr>
          <td>{{$detail->id}}</td>
          <td>{{$detail->material}}</td>
          <td>{{$detail->amount}}</td>
          <td>
            @if($detail->status_id == null)
              0
            @else
              {{$detail->given}}
            @endif
          </td>
          <td>{{$detail->unit}}</td>
          <td>
            @if($detail->status_id == 10)
              <span class="label label-primary">{{$detail->status}}</span>
            @else
              <span class="label label-danger">Negado</span>
            @endif
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <a class="btn btn-default" href="{{ route('prestamos.index') }}"> Cerrar</a>
  </div>
</div>

@endsection