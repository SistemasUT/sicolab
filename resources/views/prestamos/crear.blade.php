@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Crear préstamo de materiales</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form method="post" action="{{ route('prestamos.guardar') }}" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col-xs-6">
          <div class="form-group">
            <label for="evento">Nombre del evento:</label>
            <input type="text" class="form-control" name="evento">
          </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
          <div class="form-group">
            <label for="inicio">Fecha incio:</label>
            <input type="datetime-local" class="form-control" name="inicio"/>
          </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
          <div class="form-group">
            <label for="fin">Fecha fin:</label>
            <input type="datetime-local" class="form-control" name="fin"/>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="form-group">
            <label for="descripcion">Observaciones:</label>
            <textarea class="form-control" rows="5" name="descripcion"></textarea>
          </div>
        </div>
      </div>
      <a class="btn btn-default" href="{{ route('prestamos.index') }}"> Cancelar</a>
      <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
  </div>
</div>

@endsection