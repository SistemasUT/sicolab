@extends('layouts.app')

@section('content')
	<div class="row">
		
		<div class="offset-md-4 col-md-4">
			<div class="card">
				<div class="card-header">
					<h1 class="card-title">Acceso al Sistema</h1>
				</div>
				<div class="card-body">
					<form method="POST" action="{{ route('login') }}">
						{{ csrf_field() }}
						<div class="form-group">
							<label for="email">Email</label>
							<input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" 
								type="email" 
								name="email"
								value="{{ old('email') }}" 
								placeholder="Ingrese su email">
						{!! $errors->first('email', '<div class="invalid-feedback">:message</div>') !!}
						</div>
						<div class="form-group">
							<label for="password">Contraseña</label>
							<input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" 
								type="password"
								name="password"
								placeholder="Ingrese su contraseña">
						{!! $errors->first('password', '<div class="invalid-feedback">:message</div>') !!}
						</div>
						<button class="btn btn-primary btn-block">Acceder</button>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection