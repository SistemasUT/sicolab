@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Editar Material</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form 
      method="post" 
      action="{{ route('materiales.update', $material->id) }}" 
      enctype="multipart/form-data">
      @method('PATCH')
      @csrf
      <div class="row">
        <div class="col-xs-6">
          <div class="form-group">
            <label for="type_id">Seleccione tipo de material:</label>
            <select class="form-control" name="type_id">
              @foreach($types as $type)
                @if($type->id == $material->type_id)
                  <option selected value="{{ $type->id }}" >{{ $type->name }}</option>
                @else
                  <option value="{{ $type->id }}" >{{ $type->name }}</option>
                @endif
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-xs-6">
          <div class="form-group">
            <label for="image">Imagen:</label>
            <input type="file" class="form-control" name="image">
            <br>
            <br>
            <br>
            <img class="small" src="/img/{{$material->image}}">
            <input type="hidden" id="img_id" name="img_id" value="{{$material->image}}">
          </div>
        </div>
        <div class="col-xs-2">
          <div class="form-group">
            <label for="brand_id">Seleccione unidad:</label>
            <select class="form-control" name="unit_id">
              @foreach($units as $unit)
                @if($unit->id == $material->unit_id)
                  <option selected value="{{ $unit->id }}" >{{ $unit->name }}</option>
                @else
                  <option value="{{ $unit->id }}" >{{ $unit->name }}</option>
                @endif
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-xs-4">
          <div class="form-group">
            <label for="category_id">Seleccione categoría:</label>
            <select class="form-control" name="category_id">
              @foreach($categories as $category)
                @if($category->id == $material->brand_id)
                  <option selected value="{{ $category->id }}" >{{ $category->name }}</option>
                @else
                  <option value="{{ $category->id }}" >{{ $category->name }}</option>
                @endif
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-xs-3">
          <div class="form-group">
            <label for="brand_id">Seleccione marca:</label>
            <select class="form-control" name="brand_id">
              @foreach($brands as $brand)
                @if($brand->id == $material->brand_id)
                  <option selected value="{{ $brand->id }}" >{{ $brand->name }}</option>
                @else
                  <option value="{{ $brand->id }}" >{{ $brand->name }}</option>
                @endif
              @endforeach
            </select>
          </div>
        </div>

        <div class="col-xs-3">
          <div class="form-group">
            <label for="model">Modelo:</label>
            <input type="text" class="form-control" name="model" value="{{ $material->model }}"/>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="form-group">
            <label for="description">Descripción:</label>
            <textarea class="form-control" rows="5" name="description">{{ $material->description }}</textarea>
          </div>
        </div>
      </div>
      <a class="btn btn-default" href="{{ route('materiales.index') }}"> Cancelar</a>
      <button type="submit" class="btn btn-primary">Actualizar</button>
    </form>
  </div>
</div>

@endsection