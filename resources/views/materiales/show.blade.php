@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Ver Material</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="form-group">
      <strong>Nombre del material:</strong>
      {{ $material->type->name }}
    </div>
    <div class="form-group">
      <strong>Imagen:</strong>
    </div>
    <div class="form-group">
      <img class="small" src="{{ asset('img/'.$material->image) }}">
    </div>
    <div class="form-group">
      <strong>Unidad de medida:</strong>
      {{ $material->unit->name }}
    </div>
    <div class="form-group">
      <strong>Categoría:</strong>
      {{ $material->category->name }}
    </div>
    <div class="form-group">
      <strong>Marca:</strong>
      {{ $material->brand->name }}
    </div>
    <div class="form-group">
      <strong>Modelo:</strong>
      {{ $material->model }}
    </div>
    <div class="form-group">
      <strong>Descripción:</strong>
      {{ $material->description }}
    </div>
    <a class="btn btn-default" href="{{ route('materiales.index') }}"> Cerrar</a>
  </div>
</div>

@endsection