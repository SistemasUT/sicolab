@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Crear Material</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form method="post" action="{{ route('materiales.store') }}" enctype="multipart/form-data">
      @csrf
      <div class="form-group">
        <label for="type_id">Seleccione nombre del material:</label>
        <select class="form-control" name="type_id">
          @foreach($types as $type)
            <option value="{{ $type->id }}">{{$type->name}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="image">Imagen:</label>
        <input type="file" class="form-control" name="image">
      </div>
      <div class="form-group">
        <label for="description">Descripción:</label>
        <textarea class="form-control" rows="5" name="description"></textarea>
      </div>
      <div class="form-group">
        <label for="unit_id">Seleccione unidad de medida:</label>
        <select class="form-control" name="unit_id">
          @foreach($units as $unit)
            <option value="{{ $unit->id }}">{{$unit->name}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="category_id">Seleccione categoría:</label>
        <select class="form-control" name="category_id">
          @foreach($categories as $category)
            <option value="{{ $category->id }}">{{$category->name}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="brand_id">Seleccione marca:</label>
        <select class="form-control" name="brand_id">
          @foreach($brands as $brand)
            <option value="{{ $brand->id }}">{{$brand->name}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="model">Ingrese modelo:</label>
        <input type="text" class="form-control" name="model"/>
      </div>
      <a class="btn btn-default" href="{{ route('materiales.index') }}"> Cancelar</a>
      <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
  </div>
</div>

@endsection