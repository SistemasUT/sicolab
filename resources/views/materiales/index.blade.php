@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Catálogo de Materiales</h3>
    <div class="box-tools pull-right">
      <a class="btn btn-seccess fa fa-plus" href="{{ route('materiales.create') }}"> Agregar</a>
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table class="table table-striped">
      <thead>
          <tr>
            <td>ID</td>
            <td>Nombre del material</td>
            <td>Unidad de medida</td>
            <td>Categoría</td>
            <td>Marca</td>
            <td>Modelo</td>
            <td>Descripción</td>
            <td>Acciones</td>
          </tr>
      </thead>
      <tbody>
        @foreach($materials as $material)
          <tr>
            <td>{{$material->id}}</td>
            <td>{{$material->type->name}}</td>
            <td>{{$material->unit->name}}</td>
            <td>{{$material->category->name}}</td>
            <td>{{$material->brand->name}}</td>
            <td>{{$material->model}}</td>
            <td>{{$material->description}}</td>
            <td>
              <form action="{{ route('materiales.destroy', $material->id) }}" method="POST">
                <a class="btn btn-info fa fa-eye" href="{{ route('materiales.show',$material->id) }}"></a>
                <a class="btn btn-primary fa fa-pencil" href="{{ route('materiales.edit',$material->id) }}"></a>
                @csrf
                @method('DELETE')
                <button class="btn btn-danger fa fa-trash" type="submit"></button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    {!! $materials->links() !!}
  </div>
</div>

@endsection