@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Calendario de laboratorio</h3>
      <div class="box-tools pull-right">
        <a class="btn btn-success fa fa-calendar" href="{{ route('solicitudes.crear') }}"> Agendar</a>
      </div>
  </div>
  <div class="box-body">
    <div id='calendar'></div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
  // Se selecciona el div con el id = calendario
  $('#calendar').fullCalendar({
    // Opciones del plugin fullcalendar
    height: 470,
    weekends: false,
    header: {
      left: 'prev,next today myCustomButton',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    // Configuración del PopOver
    eventRender: function(eventObj, $el) {
      $el.popover({
        html: true,
        title: '<strong>Datos del solicitante de laboratorio</strong',
        content: 'Solicitante: '+eventObj.title
                +'</br> Asignatura: '+eventObj.subject
                +'</br> Laboratorio: '+eventObj.category
                +'</br> De : '+eventObj.start.format("h:mm A")
                +' a '+eventObj.end.format("h:mm A"),
        trigger: 'hover',
        placement: 'top',
        container: 'body'
      });
    },
    // Listado de los eventos aceptados
    events : [
        @foreach($details as $detail){
          @if($detail->status_id == 6)
            id: '{{ $detail->id }}',
            title : '{{ $detail->user }}',
            subject: '{{ $detail->subject }}',
            category: '{{ $detail->category }}',
            start : '{{ $detail->date.' '.$detail->starts }}',
            end: '{{ $detail->date.' '.$detail->ends }}',
          @endif
        },
        @endforeach
    ]
  });
</script>
@endsection