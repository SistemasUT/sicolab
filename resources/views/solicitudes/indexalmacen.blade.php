@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Solicitud de laboratorio</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table class="table table-striped">
      <thead>
        <tr>
          <td>ID</td>
          <td>Solicitante</td>
          <td>Solicitud de</td>
          <td>Asignatura de</td>
          <td>Horario</td>
          <td>Status</td>
          <td>Acciones</td>
        </tr>
      </thead>
      <tbody>
        @foreach($details as $detail)
          <tr>
            <td>{{$detail->id}}</td>
            <td>{{$detail->user}}</td>
            <td>{{$detail->category}}</td>
            <td>{{$detail->subject}}</td>
            <td>
              {{'El '.Jenssegers\Date\Date::parse($detail->date)->format('d').' de '}}
              {{Jenssegers\Date\Date::parse($detail->date)->format('F').' del '}}
              {{Jenssegers\Date\Date::parse($detail->date)->format('Y')}}
              {{' de '.Carbon\Carbon::parse($detail->starts)->format('g:i A')}}
              {{' a '.Carbon\Carbon::parse($detail->ends)->format('g:i A')}}
            </td>
            <td>
              @if($detail->status_id == 5)
                <span class="label label-primary">{{$detail->status}}</span>
              @else
                <span class="label label-success">{{$detail->status}}</span>
              @endif
            </td>
            <td>
              @if($detail->status_id == 5)
                <form action="{{ route('solicitudes.eliminar', $detail->id) }}" method="POST">
                  @csrf
                  @method('DELETE')
                    <a class="btn btn-success fa fa-check" href="{{ route('solicitudes.aceptar',$detail->id) }}"></a>
                    <button class="btn btn-danger fa fa-close" type="submit"></button>
                </form>
              @endif
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection