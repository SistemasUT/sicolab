<!-- Extiende de la plantilla principal AdminLTE -->
@extends('dashboard')
<!-- Sección para el contenido del módulo -->
@section('content')
<!-- Inicio del contenedor box -->
<div class="box box-solid box-success">
  <!-- Contenedor del encabezado del elemento box -->
  <div class="box-header with-border">
    <h3 class="box-title">Solicitud de laboratorio</h3>
  </div>
  <!-- Contenedor del cuerpo del elemento box -->
  <div class="box-body">
    <!-- Formulario para la solicitud de espacios de trabajo -->
    <form form method="post" action="{{ route('solicitudes.añadir') }}">
    @csrf
      <input type="hidden" name="user_id" value="{{ $user }}">
      <!-- Inicio de un elemento en rejillas --->
      <div class="row">
        <!-- Select de asignaturas -->
        <div class="col-xs-3 col-sm-3 col-md-3">
          <div class="form-group">
            <label for="subject_id">Seleccione la asignatura:</label>
            <select class="form-control" name="subject_id">
              @foreach($subjects as $subject)
                <option value="{{ $subject->id }}">{{$subject->name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <!-- Select de asignaturas -->
        <div class="col-xs-3 col-sm-3 col-md-3">
          <div class="form-group">
            <label for="category_id">Seleccione el laboratorio:</label>
            <select class="form-control" name="category_id">
              @foreach($laboratories as $laboratory)
                <option value="{{ $laboratory->id }}">{{$laboratory->name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2">
          <div class="form-group">
            <label for="fecha">Fecha:</label>
              <input id="fecha" type="date" class="form-control" name="fecha"/>
          </div>
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2">
          <div class="form-group">
            <label for="inicio">Hora inicio:</label>
              <input id="inicio" type="time" class="form-control" name="inicio"/>
          </div>
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2">
          <div class="form-group">
            <label for="fin">Hora fin:</label>
              <input id="fin" type="time" class="form-control" name="fin"/>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="pull-right">
            <a class="btn btn-default" href="{{ route('solicitudes.index') }}">Volver</a>
            <button type="submit" class="btn btn-primary">Solicitar</button>
          </div>
        </div>
      </div>
    </form>
  </br>
    <table class="table table-striped">
      <thead>
        <tr>
          <td>ID</td>
          <td>Solicitud de</td>
          <td>Asignatura de</td>
          <td>Horario</td>
          <td>Status</td>
          <td>Acciones</td>
        </tr>
      </thead>
      <tbody>
        @foreach($details as $detail)
          <tr>
            <td>{{$detail->id}}</td>
            <td>{{$detail->category}}</td>
            <td>{{$detail->subject}}</td>
            <td>
              {{'El '.Jenssegers\Date\Date::parse($detail->date)->format('d').' de '}}
              {{Jenssegers\Date\Date::parse($detail->date)->format('F').' del '}}
              {{Jenssegers\Date\Date::parse($detail->date)->format('Y')}}
              {{' de '.Carbon\Carbon::parse($detail->starts)->format('g:i A')}}
              {{' a '.Carbon\Carbon::parse($detail->ends)->format('g:i A')}}
            </td>
            <td>
              @if($currentDate <= strtotime($detail->date))
                @if($detail->status_id == 5)
                  <span class="label label-primary">{{$detail->status}}</span>
                @else
                  <span class="label label-success">{{$detail->status}}</span>
                @endif
              @else
                <span class="label label-danger">Finalizado</span>
              @endif
            </td>
            <td>
              @if($detail->status_id == 5)
                <form action="{{ route('solicitudes.eliminar', $detail->id) }}" method="POST">
                  @csrf
                  @method('DELETE')
                    <button class="btn btn-danger fa fa-trash" type="submit"></button>
                </form>
              @endif
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection