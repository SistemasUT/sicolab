@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Catálogo de Asignaturas</h3>
    <div class="box-tools pull-right">
      <a class="btn btn-success fa fa-plus" href="{{ route('asignaturas.create') }}"> Agregar</a>
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table class="table table-striped">
      <thead>
          <tr>
            <td>ID</td>
            <td>Nombre de la asignatura</td>
            <td>Descripción</td>
            <td>Acciones</td>
          </tr>
      </thead>
      <tbody>
        @foreach($asignaturas as $asignatura)
          <tr>
            <td>{{$asignatura->id}}</td>
            <td>{{$asignatura->name}}</td>
            <td>{{$asignatura->description}}</td>
            <td>
              <form action="{{ route('asignaturas.destroy', $asignatura->id) }}" method="POST">
                <a class="btn btn-info fa fa-eye" href="{{ route('asignaturas.show',$asignatura->id) }}"></a>
                <a class="btn btn-primary fa fa-pencil" href="{{ route('asignaturas.edit',$asignatura->id) }}"></a>
                @csrf
                @method('DELETE')
                <button class="btn btn-danger fa fa-trash" type="submit"></button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    {!! $asignaturas->links() !!}
  </div>
</div>

@endsection