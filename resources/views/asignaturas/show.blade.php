@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Ver Asignatura</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="form-group">
      <strong>Nombre de la asignatura:</strong>
      {{ $asignatura->name }}
    </div>
    <div class="form-group">
      <strong>Descripción:</strong>
      {{ $asignatura->description }}
    </div>
    <a class="btn btn-default" href="{{ route('asignaturas.index') }}"> Cerrar</a>
  </div>
</div>

@endsection