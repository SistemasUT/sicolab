<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Login</title>
	<link rel="stylesheet" href="{{ asset('/css/app.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/login.css') }}">
	
</head>
<body id="login" style="background-image: url( {{ asset('img/login.jpg') }} )">
	<div class="container" style="background: color">
		<br>
		
		@if(session()->has('flash'))
			<div class="alert alert-info">{{ session('flash') }}</div>
		@endif

		@yield('content')

	</div>
</body>
</html>