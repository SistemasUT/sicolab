@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Catálogo de Direcciones/Áreas</h3>
    <div class="box-tools pull-right">
      <a class="btn btn-success fa fa-plus" href="{{ route('areas.create') }}"> Agregar</a>
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table class="table table-striped">
      <thead>
          <tr>
            <td>ID</td>
            <td>Nombre del área</td>
            <td>Descripción</td>
            <td>Acciones</td>
          </tr>
      </thead>
      <tbody>
        @foreach($areas as $area)
          <tr>
            <td>{{$area->id}}</td>
            <td>{{$area->name}}</td>
            <td>{{$area->description}}</td>
            <td>
              <form action="{{ route('areas.destroy', $area->id) }}" method="POST">
                <a class="btn btn-info fa fa-eye" href="{{ route('areas.show',$area->id) }}"></a>
                <a class="btn btn-primary fa fa-pencil" href="{{ route('areas.edit',$area->id) }}"></a>
                @csrf
                @method('DELETE')
                <button class="btn btn-danger fa fa-trash" type="submit"></button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    {!! $areas->links() !!}
  </div>
</div>

@endsection