@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Ver Área/Dirección</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="form-group">
      <strong>Nombre del área:</strong>
      {{ $area->name }}
    </div>
    <div class="form-group">
      <strong>Descripción:</strong>
      {{ $area->description }}
    </div>
    <a class="btn btn-default" href="{{ route('areas.index') }}"> Cerrar</a>
  </div>
</div>

@endsection