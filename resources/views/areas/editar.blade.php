@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Crear Área/Dirección</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form method="post" action="{{ route('areas.update', $area->id) }}">
      @method('PATCH')
      @csrf
      <div class="form-group">
        <label for="name">Nombre del área:</label>
        <input type="text" class="form-control" name="name" value="{{ $area->name }}" />
      </div>
      <div class="form-group">
        <label for="description">Descripción:</label>
        <textarea class="form-control" rows="5" name="description">{{ $area->description }}</textarea>
      </div>
      <a class="btn btn-default" href="{{ route('areas.index') }}"> Cancelar</a>
      <button type="submit" class="btn btn-primary">Actualizar</button>
    </form>
  </div>
</div>

@endsection