@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Catálogo de Partidas</h3>
    <div class="box-tools pull-right">
      <a class="btn btn-success fa fa-plus" href="{{ route('partidas.create') }}"> Agregar</a>
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table class="table table-striped">
      <thead>
          <tr>
            <td>ID</td>
            <td>Nombre del área</td>
            <td>Descripción</td>
            <td>Acciones</td>
          </tr>
      </thead>
      <tbody>
        @foreach($departures as $departure)
          <tr>
            <td>{{$departure->id}}</td>
            <td>{{$departure->name}}</td>
            <td>{{$departure->description}}</td>
            <td>
              <form action="{{ route('partidas.destroy', $departure->id) }}" method="POST">
                <a class="btn btn-info fa fa-eye" href="{{ route('partidas.show',$departure->id) }}"></a>
                <a class="btn btn-primary fa fa-pencil" href="{{ route('partidas.edit',$departure->id) }}"></a>
                @csrf
                @method('DELETE')
                <button class="btn btn-danger fa fa-trash" type="submit"></button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    {!! $departures->links() !!}
  </div>
</div>

@endsection