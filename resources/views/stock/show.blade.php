@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Material en stock</h3>
    <div class="box-tools pull-right">
      {{--<a class="btn btn-success fa fa-plus" href="{{ route('stock.create') }}"> Agregar</a>--}}
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
      <div class="col-xs-6">      
        <div class="form-group">
          <strong>Nombre del material:</strong>
            </br>
            {{ $itemStock->material->type->name }}
        </div>
      </div>
      <div class="col-xs-6">
        <div class="form-group">
          <strong>Número de serie:</strong>
            </br>
            {{ $itemStock->serial }}
        </div>
      </div>
      <div class="col-xs-6">
        <div class="form-group">
          <strong>Unidad:</strong>
            </br>
            {{ $itemStock->material->unit->name }}
        </div>
      </div>
      <div class="col-xs-6">
        <div class="form-group">
          <strong>Marca:</strong>
            </br>
            {{ $itemStock->material->brand->name }}
        </div>
      </div>
      <div class="col-xs-6">
        <div class="form-group">
          <strong>Modelo:</strong>
            </br>
            {{ $itemStock->material->model }}
        </div>
      </div>
      <div class="col-xs-3">
        <div class="form-group">
          <strong>Imagen:</strong>
            </br>
            <img class="small" src="{{ asset('img/'.$itemStock->material->image) }}">
        </div>
      </div>
      <div class="col-xs-3">
        <div class="form-group">
          <strong>Código QR:</strong>
            </br>
            <img src="data:image/png;base64, 
              {!! base64_encode(QrCode::format('png')
                ->encoding('UTF-8')
                ->size(150)
                ->generate(
                  'Nombre del material: '.$itemStock->material->type->name.', '.
                  'Número de serie: '.$itemStock->serial.' , '.
                  'Unidad de medida: '.$itemStock->material->unit->name.', '.
                  'Marca: '.$itemStock->material->brand->name.' , '.
                  'Modelo: '.$itemStock->material->model)
              )!!} ">
        </div>
      </div>
    </div>
    <a class="btn btn-default" href="{{ route('stock.index') }}"> Cerrar</a>
  </div>
</div>

@endsection