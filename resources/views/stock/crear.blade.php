@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Almacenar material</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form method="post" action="{{ route('stock.store') }}">
      @csrf
      <div class="form-group">
        <label for="material_id">Seleccione nombre del material:</label>
        <select class="form-control" name="material_id">
          @foreach($materials as $material)
            <option value="{{ $material->id }}">{{$material->type->name}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="serial">Número de serie:</label>
        <input type="text" class="form-control" name="serial"/>
      </div>
      <div class="form-group">
        <label for="expiration">Fecha de expiración:</label>
        <input type="date" class="form-control" name="expiration"/>
      </div>
      <a class="btn btn-default" href="{{ route('stock.index') }}"> Cancelar</a>
      <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
  </div>
</div>

@endsection