@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Editar Modelo</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form method="post" action="{{ route('materiales.update', $material->id) }}">
      @method('PATCH')
      @csrf
      <div class="form-group">
        <label for="name">Nombre del material:</label>
        <input type="text" class="form-control" name="name" value="{{ $material->name }}"/>
      </div>
      <div class="form-group">
        <label for="key">Clave:</label>
        <input type="text" class="form-control" name="key" value="{{ $material->key }}"/>
      </div>
      <div class="form-group">
        <label for="unit">Cantidad:</label>
        <input type="number" class="form-control" name="unit" value="{{ $material->unit }}"/>
      </div>
      <div class="form-group">
        <label for="code">Código:</label>
        <input type="number" class="form-control" name="code" value="{{ $material->code }}"/>
      </div>
      <div class="form-group">
        <label for="description">Descripción:</label>
        <textarea class="form-control" rows="5" name="description">{{ $material->description }}</textarea>
      </div>
      <div class="form-group">
        <label>Seleccione marca:</label>
        <select class="form-control" name="brand_id">
          @foreach($brands as $brand)
            @if($brand->id == $material->brand_id)
              <option selected value="{{ $brand->id }}" >{{ $brand->name }}</option>
            @else
              <option value="{{ $brand->id }}" >{{ $brand->name }}</option>
            @endif
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label>Seleccione marca:</label>
        <select class="form-control" name="exemplar_id">
          @foreach($exemplars as $exemplar)
            @if($exemplar->id == $material->exemplar_id)
              <option selected value="{{ $exemplar->id }}" >{{ $exemplar->name }}</option>
            @else
              <option value="{{ $exemplar->id }}" >{{ $exemplar->name }}</option>
            @endif
          @endforeach
        </select>
      </div>
      <a class="btn btn-default" href="{{ route('materiales.index') }}"> Cancelar</a>
      <button type="submit" class="btn btn-primary">Actualizar</button>
    </form>
  </div>
</div>

@endsection