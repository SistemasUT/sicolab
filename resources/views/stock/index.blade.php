@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Almacén</h3>
    <div class="box-tools pull-right">
      <a class="btn btn-success fa fa-plus" href="{{ route('stock.create') }}"> Agregar</a>
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  {{--<form action="{{ route('materiales.destroy', $material->id) }}" method="POST">--}}
    <div class="box-body">
      {{--<div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="area_id">Seleccione categoría:</label>
            <select class="form-control" name="area_id">
              @foreach($categories as $category)
                <option value="{{ $category->id }}">{{$category->name}}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>--}}
      <table class="table table-striped">
        <thead>
            <tr>
              <td>ID</td>
              <td>Nombre del material</td>
              <td>Número de serie</td>
              <td>Unidad</td>
              <td>Marca</td>
              <td>Modelo</td>
              <td>Acciones</td>
            </tr>
        </thead>
        <tbody>
          @foreach($stocks as $stock)
            <tr>
              <td>{{$stock->id}}</td>
              <td>{{$stock->material->type->name}}</td>
              <td>{{$stock->serial}}</td>
              <td>{{$stock->material->unit->name}}</td>
              <td>{{$stock->material->brand->name}}</td>
              <td>{{$stock->material->model}}</td>
              <td>
                <form action="{{ route('stock.destroy', $stock->id) }}" method="POST">
                  <a class="btn btn-info fa fa-eye" href="{{ route('stock.show',$stock->id) }}"></a>
                  <a class="btn btn-primary fa fa-pencil" href="{{ route('marcas.edit',$stock->id) }}"></a>
                  @csrf
                  @method('DELETE')
                    <button class="btn btn-danger fa fa-trash" type="submit"></button>
                </form>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
      {!! $stocks->links() !!}
    </div>
  {{-- </form> --}}
</div>

@endsection