<style>
    .modal {
        text-align: center;
    }
    .modal-dialog {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%) !important;
    }
    .modal-dialog .icon-box {
        width: 80px;
        height: 80px;
        margin: 0 auto;
        border-radius: 50%;
        z-index: 9;
        text-align: center;
        border: 3px solid #ffff;
    }
    .modal-dialog .icon-box i {
        color: #ffff;
        font-size: 46px;
        display: inline-block;
        margin-top: 13px;
    }
    .modal-dialog .modal-body h4 {
        font-weight: bold;
    }
</style>

<div class="modal modal-danger fade" id="delete-model">
    <div class="modal-dialog">
        <div class="modal-content alert-danger">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <div class="icon-box">
                    <i class="fa fa-close"></i>
                </div>
            </div>
            <div class="modal-body">
                <h4>¿Seguro que desea eliminar el modelo {{ $exemplar->name }}?</h4>
                <p>Este proceso no se puede deshacer...</p>
            </div>
            <div class="modal-footer">
                <form action="{{ route('marcas.destroy', $exemplar->id) }}" method="post">
                    @csrf
                    @method('DELETE')
                    <button type="button" class="btn btn-outline" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-outline" type="submit">Eliminar</button>
              </form>
            </div>
        </div>
    </div>
</div>