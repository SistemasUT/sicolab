
<div class="modal modal-danger fade" id="deletebrand">
    <div class="modal-dialog">
        <div class="modal-content alert-danger">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <div class="icon-box">
                    <i class="fa fa-close"></i>
                </div>
            </div>
            <div class="modal-body">
                <h4>¿Seguro que desea eliminar la marca {{ $brand->name }}?</h4>
                <p>Este proceso no se puede deshacer...</p>
            </div>
            <div class="modal-footer">
                <form action="{{ route('marcas.destroy', $brand->id) }}" method="post">
                    @csrf
                    @method('DELETE')
                    
                    <button class="btn btn-outline" type="submit">Eliminar</button>
              </form>
            </div>
        </div>
    </div>
</div>