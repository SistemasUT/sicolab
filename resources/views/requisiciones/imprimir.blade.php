<?php
  header("Content-type:application/vnd.ms-excel");
  header("Content-Disposition:attachment;filename = Requisicion.xls");
?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta charset=UTF-8">
  <style type="text/css">
    html{
      color: #4B4B4B;
    }
    body {
      font-family: Arial;
    }
    img {
      display: block;
      margin-left: auto;
      margin-right: auto;
    }
    .requisitionBody, .requisitionHead, th, td {
        border: 1px solid #ccccb3;
    }

    .address td {
        border: 0px;
    }
    
    .requisitionBody, .requisitionHead, .address {
      border-collapse: collapse;
      margin: 0 auto;
    }
    th {
      background-color: #01AF8C;
    }
    .requisitionHead th {
      text-align: right;
      width: 400px;
    }
    .requisitionHead td {
      text-align: center;
    }
    .requisitionBody th {
      height: 50px;
    }
    .requisitionBody tr td {
      font-size: 12px;
    }
    h3 {
      text-align: center;
    }
    .address {
      text-align: center;
      font-size: 10px;
    }
  </style>
</head>
<body>
  <img src="{{ asset('img/bannerexcel.gif') }}">
  <br>
  <br>
  <br>
  <br>
  <h3>Requisiciones de Material y/o Servicios</h3>
  <table class="requisitionHead">
    <thead>
      <tr>
        <th colspan="3">Dirección/Área:</th>
        <th colspan="8" style="background-color: #ffffff; font-weight: normal; text-align: center;">{{$requisition->area->name}}</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td colspan="3" style="background-color: #01AF8C; text-align: right; font-weight: bold;">Nombre del solicitante:</td>
        <td colspan="8">{{$requisition->user->name}}</td>
      </tr>
      <tr>
        <td colspan="3" style="background-color: #01AF8C; text-align: right; font-weight: bold;">Por concepto de:</td>
        <td colspan="8">{{$requisition->concept}}</td>
      </tr>
      <tr>
        <td colspan="3" style="background-color: #01AF8C; text-align: right; font-weight: bold;">Nombre del evento:</td>
        <td colspan="8">{{$requisition->event}}</td>
      </tr>
      <tr>
        <td colspan="3" style="background-color: #01AF8C; text-align: right; font-weight: bold;">Fecha:</td>
        <td colspan="8">{{$requisition->created_at}}</td>
      </tr>
      <tr>
        <td colspan="3" style="background-color: #01AF8C; text-align: right; font-weight: bold;">Folio:</td>
        <td colspan="8">{{$requisition->folio}}</td>
      </tr>
    </tbody>
  </table>
  <br>
  <br>
  <table class="requisitionBody">
    <thead>
        <tr>
          <th>No.</th>
          <th colspan="2">Folio del Componente</th>
          <th>Núm. Partida</th>
          <th>Cantidad</th>
          <th>Unidad</th>
          <th>Concepto</th>
          <th colspan="4">Observaciones</th>
        </tr>
    </thead>
    <tbody>
      <?php
        $cont = 1;
      ?>
      @foreach($requisitionsDetails as $requisitionDetail)
        <tr id="{{$requisitionDetail->id}}">
          <td style="text-align: center;">1</td>
          <td colspan="2">{{$requisitionDetail->component}}</td>
          <td>{{$requisitionDetail->departure}}</td>
          <td>{{$requisitionDetail->amount}}</td>
          <td>{{$requisitionDetail->unit}}</td>
          <td>{{$requisitionDetail->material}}</td>
          <td colspan="4">{{$requisitionDetail->description}}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
  <br>
    <table class="address">
      <tbody>
      <tr>
        <td colspan="2"></td>
        <td colspan="6" style="text-align: center;">
          <strong>Universidad Tecnológica de Chetumal</strong><br>
          Teléfono: (983) 1291765Correo Electrónico: facturas@utchetumal.edu.mx<br>
          Camino Antiguo a Santa Elena, sin Col. Centro<br>
          <strong>Organismo Público Descentralizado del Gobierno del Estado de Quintana Roo</strong>
        </td>
        <td colspan="3" style="font-weight: bold; text-align: right;">
          F-ADM-002-REV.03<br>
          10/01/2018
        </td>
      </tr>
      </tbody>
    </table>
  <img src="{{ asset('img/footer.gif') }}">
</body>
</html>