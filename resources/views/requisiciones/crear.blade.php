@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Crear Requisición</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form method="post" action="{{ route('requisiciones.guardar') }}" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col-xs-6">      
          <div class="form-group">
            <label for="area_id">Seleccione dirección solicitante:</label>
            <select class="form-control" name="area_id">
              @foreach($areas as $area)
                <option value="{{ $area->id }}">{{$area->name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-xs-6">
          <div class="form-group">
            <label for="concepto">Por concepto de:</label>
            <input type="text" class="form-control" name="concepto">
          </div>
        </div>
        <div class="col-xs-6">
          <div class="form-group">
            <label for="evento">Nombre del evento:</label>
            <input type="text" class="form-control" name="evento">
          </div>
        </div>
        <div class="col-xs-6">
          <div class="form-group">
            <label for="folio">Folio:</label>
            <input type="text" class="form-control" name="folio">
          </div>
        </div>
      </div>
      <a class="btn btn-default" href="{{ route('requisiciones.index') }}"> Cancelar</a>
      <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
  </div>
</div>

@endsection