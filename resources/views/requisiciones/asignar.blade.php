@extends('dashboard')

@section('content')

<script src="{{ asset('js/app.js') }}"></script>

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Detalles de la requisición</h3>
    <div class="box-tools pull-right">
      <a class="btn btn-success fa fa-search" data-toggle="modal" data-target=".bd-example-modal-lg"> Buscar</a>
      <a class="btn btn-success fa fa-plus" onclick="addNewMaterial()"> Agregar</a>
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
      <div class="col-xs-6">
        <p><strong>Dirección/Área: </strong> {{ $requisition->area->name }}</p>
        <p><strong>Por concepto de: </strong> {{ $requisition->concept }}</p>
        <p><strong>Fecha: </strong> {{ $requisition->created_at }}</p>
      </div>
      <div class="col-xs-6">
        <p><strong>Nombre del solicitante: </strong> {{ $requisition->user->name }}</p>
        <p><strong>Nombre del evento: </strong> {{ $requisition->event }}</p>
        <p><strong>Folio: </strong> {{ $requisition->folio }}</p>
      </div>
    </div>
    <!--Formulario de la requisición-->
    <form method="post" action="{{ route('requisiciones.detalles.guardar') }}" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="requisition_id" value="{{ $requisitionID }}">
    <table id="requisitionsTable" class="table table-striped">
      <thead>
          <tr>
            <td>ID</td>
            <td>Folio del Componente</td>
            <td>Núm de Partida</td>
            <td>Cantidad</td>
            <td>Unidad</td>
            <td>Concepto</td>
            <td>Acciones</td>
          </tr>
      </thead>
      <tbody>
        @foreach($requisitionsDetails as $requisitionDetail)
          <tr id="{{$requisitionDetail->id}}">
            <td></td>
            <td><input type="text" class="form-control" name="component[]" value="{{$requisitionDetail->component}}"/></td>
            <td>
              <select class="form-control" name="departure_id[]">
                @foreach($departures as $departure)
                  @if($departure->id == $requisitionDetail->departure_id)
                    <option selected value="{{ $departure->id }}">{{ $departure->name }}</option>
                  @else
                    <option value="{{ $departure->id }}">{{ $departure->name }}</option>
                  @endif
                @endforeach
              </select>
            </td>
            <td><input type="number" class="form-control" name="amount[]" min="1" value="{{$requisitionDetail->amount}}"/></td>
            <td>{{$requisitionDetail->unit}}</td>
            <td>{{$requisitionDetail->material}}<input type="hidden" name="stock_id[]" value="{{$requisitionDetail->material_id}}"/></td>
            <td>
              <button class="btn btn-danger fa fa-trash" type="button" onclick="deleteRow({{$requisitionDetail->id}})"></button>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <a class="btn btn-default" href="{{ route('requisiciones.index') }}">Regresar</a>
    <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
  </div>
</div>
<!-- Busqueda de materiales -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Busqueda de materiales</h3>
      </div>
      <div class="modal-body">
        <div class="input-group">
          <span class="input-group-addon">Buscar</span>
          <input id="finder" type="text" class="form-control" placeholder="Ingrese el nombre del material a buscar...">
        </div>
        <br>
        <table class="table table-striped">
          <thead>
            <tr>
              <td>ID</td>
              <td>Nombre del material</td>
              <td>Cantidad</td>
              <td>Unidad</td>
              <td>Acciones</td>
            </tr>
          </thead>
          <tbody class="find">
            @foreach($stocks as $stock)
            <tr  id="{{$stock->id}}">
              <td>{{$stock->id}}</td>
              <td>{{$stock->name}}</td>
              <td>{{$stock->amount}}</td>
              <td>{{$stock->unit}}</td>
              <td>
                <a class="btn btn-primary fa fa-plus" onclick="addMaterial({{$stock->id}})"></a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function(){
    (function($){
      $('#finder').keyup(function(){
        var rex = new RegExp($(this).val(), 'i');
        $('.find tr').hide();
        $('.find tr').filter(function(){
            return rex.test($(this).text());
        }).show();
      })
    }(jQuery));
  });

  function addMaterial(id){
    var name = document.getElementById(id).cells[1].innerHTML;
    var amount = document.getElementById(id).cells[2].innerHTML;
    var unit = document.getElementById(id).cells[3].innerHTML;
    var stockID = document.getElementById(id).cells[0].innerHTML;
   
    var newRow="<tr>";
        newRow+="<td></td>";
        newRow+="<td><input type='text' class='form-control' name='component[]'></td>";
        newRow+="<td><select class='form-control' name='departure_id[]'>@foreach($departures as $departure)<option value='{{$departure->id}}'>{{$departure->name}}</option>@endforeach</select></td>";
        newRow+="<td><input type='number' class='form-control' name='amount[]' min='1' value='"+amount+"'></td>";
        newRow+="<td>"+unit+"</td>";
        newRow+="<td>"+name+"<input type='hidden' name='stock_id[]' value='"+stockID+"'></td>";
        newRow+="<td><button class='btn btn-danger fa fa-trash' type='button' onclick='deleteRow()'></button></td>";
        newRow+="</tr>";
    $("#requisitionsTable").append(newRow);
  }

  function addNewMaterial(){
    var newRow="<tr>";
        newRow+="<td></td>";
        newRow+="<td><input type='text' class='form-control' name='component[]' value=''></td>";
        newRow+="<td><select class='form-control' name='departure_id[]'>@foreach($departures as $departure)<option value='{{$departure->id}}'>{{$departure->name}}</option>@endforeach</select></td>";
        newRow+="<td><input type='number' class='form-control' min='1' name='amount[]'/></td>";
        newRow+="<td></td>";
        newRow+="<td><select class='form-control' name='stock_id[]'>@foreach($materials as $material)<option value='{{$material->id}}'>{{$material->type->name}}</option>@endforeach</select></td>";
        newRow+="<td><button class='btn btn-danger fa fa-trash' type='button' onclick='deleteRow()'></button></td>";
        newRow+="</tr>";
    $("#requisitionsTable").append(newRow);
  }

  function deleteRow(rowid){ 
    var row = document.getElementById(rowid);
    row.parentNode.removeChild(row);
  }
</script>

@endsection