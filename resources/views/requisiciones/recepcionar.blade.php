@extends('dashboard')

@section('content')

<script src="{{ asset('js/app.js') }}"></script>

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Detalles de la requisición</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
      <div class="col-xs-6">
        <p><strong>Dirección/Área: </strong> {{ $requisition->area->name }}</p>
        <p><strong>Por concepto de: </strong> {{ $requisition->concept }}</p>
        <p><strong>Fecha: </strong> {{ $requisition->created_at }}</p>
      </div>
      <div class="col-xs-6">
        <p><strong>Nombre del solicitante: </strong> {{ $requisition->user->name }}</p>
        <p><strong>Nombre del evento: </strong> {{ $requisition->event }}</p>
        <p><strong>Folio: </strong> {{ $requisition->folio }}</p>
      </div>
    </div>
    <!--Formulario de la requisición-->
    <form method="post" action="{{ route('recepcionar.guardar') }}" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="requisition_id" value="{{ $requisitionID }}">
    <table id="requisitionsTable" class="table table-striped">
      <thead>
          <tr>
            <td>ID</td>
            <td>Folio del Componente</td>
            <td>Núm de Partida</td>
            <td>Cant. solicitada</td>
            <td>Cant. recibida</td>
            <td>Unidad</td>
            <td>Concepto</td>
            <td>Observaciones</td>
            <td>Acciones</td>
          </tr>
      </thead>
      <tbody>
        @foreach($requisitionsDetails as $requisitionDetail)
          <tr id="{{$requisitionDetail->id}}">
            <td><input type="hidden" name="id[]" value="{{$requisitionDetail->id}}"/></td>
            <td>{{$requisitionDetail->component}}<input type="hidden" name="component[]" value="{{$requisitionDetail->component}}"/></td>
            <td>{{$requisitionDetail->departure}}<input type="hidden" name="departure_id[]" value="{{$requisitionDetail->departure_id}}"/></td>
            <td>{{$requisitionDetail->amount}}<input type="hidden" name="amount[]" value="{{$requisitionDetail->amount}}"/></td>
            <td>
              @if($requisitionDetail->status_id == 3 || $requisitionDetail->status_id == 9)
                {{$requisitionDetail->receive}}<input type="hidden" name="receive[]" value="{{$requisitionDetail->receive}}"/>
              @else
                @if($requisitionDetail->receive == null)
                  <input type="number" class="form-control" name="receive[]" min="1" max="{{$requisitionDetail->amount}}" value="{{$requisitionDetail->amount}}"/>
                @else
                  <input type="number" class="form-control" name="receive[]" min="1" max="{{$requisitionDetail->amount}}" value="{{$requisitionDetail->receive}}"/>
                @endif
              @endif
            </td>
            <td>{{$requisitionDetail->unit}}</td>
            <td>{{$requisitionDetail->material}}<input type="hidden" name="material_id[]" value="{{$requisitionDetail->material_id}}"/></td>
            <td>
              @if($requisitionDetail->status_id == 3 || $requisitionDetail->status_id == 9)
                {{$requisitionDetail->description}}
                <input type="hidden" name="description[]" value="{{$requisitionDetail->description}}" />
              @else
                <textarea class="form-control" rows="1" name="description[]">{{$requisitionDetail->description}}</textarea>
              @endif
            </td>
            <td>
              @if($requisitionDetail->status_id == 3)
                <span class="label label-primary">{{$requisitionDetail->status}}</span>
                <input type="hidden" name="status_id[]" value="{{ $requisitionDetail->id."_".$requisitionDetail->status_id }}"/>
              @elseif($requisitionDetail->status_id == 9)
                <span class="label label-success">{{$requisitionDetail->status}}</span>
                <input type="hidden" name="status_id[]" value="{{ $requisitionDetail->id."_".$requisitionDetail->status_id }}"/>
              @else
                <label class="switch">
                  <input name="status_id[]" value="{{ $requisitionDetail->id."_"."3" }}" type="checkbox">
                  <span class="slider round"></span>
                </label>
               @endif
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <a class="btn btn-default" href="{{ route('requisiciones.index') }}">Regresar</a>
    <button type="submit" class="btn btn-primary">Guardar</button>
    <a class="btn btn-danger" href="{{ route('requisiciones.enviar',$requisition->id) }}">Cerrar requisición</a>
    </form>
  </div>
</div>

@endsection