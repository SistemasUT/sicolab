@extends('dashboard')

@section('content')

<script src="{{ asset('js/app.js') }}"></script>

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Detalles de la requisición</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
      <div class="col-xs-6">
        <p><strong>Dirección/Área: </strong> {{ $requisition->area->name }}</p>
        <p><strong>Por concepto de: </strong> {{ $requisition->concept }}</p>
        <p><strong>Fecha: </strong> {{ $requisition->created_at }}</p>
      </div>
      <div class="col-xs-6">
        <p><strong>Nombre del solicitante: </strong> {{ $requisition->user->name }}</p>
        <p><strong>Nombre del evento: </strong> {{ $requisition->event }}</p>
        <p><strong>Folio: </strong> {{ $requisition->folio }}</p>
      </div>
    </div>
    <!--Formulario de la requisición-->
    <form method="post" action="{{ route('requisiciones.detalles.guardar') }}" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="requisition_id" value="{{ $requisitionID }}">
    <table id="requisitionsTable" class="table table-striped">
      <thead>
          <tr>
            <td>ID</td>
            <td>Folio del Componente</td>
            <td>Núm de Partida</td>
            <td>Cant. enviada</td>
            <td>Cant. recibida</td>
            <td>Unidad</td>
            <td>Concepto</td>
            <td>Status</td>
            <td>Acciones</td>
          </tr>
      </thead>
      <tbody>
        @foreach($requisitionsDetails as $requisitionDetail)
          <tr id="{{$requisitionDetail->id}}">
            <td><input type="hidden" name="requisitionDetail[]" value="{{ $requisitionDetail->id }}"></td>
            <td>{{$requisitionDetail->component}}<input type="hidden" name="component[]" value="{{$requisitionDetail->component}}"/></td>
            <td>{{ $requisitionDetail->departure }}<input type="hidden" name="departure_id[]" value="{{$requisitionDetail->departure_id}}"/></td>
            <td>{{$requisitionDetail->receive}}<input type="hidden" name="receive[]" value="{{$requisitionDetail->receive}}"/></td>
            <td>
              @if($requisitionDetail->status_id == 3)
                @if($requisitionDetail->storage == null)
                  <input type="number" class="form-control" name="storage[]" min="1" max="{{$requisitionDetail->receive}}" value="{{$requisitionDetail->receive}}"/>
                @else
                  <input type="number" class="form-control" name="storage[]" min="1" max="{{$requisitionDetail->receive}}" value="{{$requisitionDetail->storage}}"/>
                @endif
              @elseif($requisitionDetail->status_id == 9)
                {{$requisitionDetail->storage}}<input type="hidden" name="storage[]" value="{{$requisitionDetail->storage}}"/>
              @endif
            </td>
            <td>{{$requisitionDetail->unit}}</td>
            <td>{{$requisitionDetail->material}}<input type="hidden" name="stock_id[]" value="{{$requisitionDetail->material_id}}"/></td>
            <td>
              @if($requisitionDetail->status_id == 3)
                <span class="label label-primary">{{$requisitionDetail->status}}</span>
              @elseif($requisitionDetail->status_id == 9)
                <span class="label label-success">{{$requisitionDetail->status}}</span>
              @endif
            </td>
            <td>
              {{--<a class="btn btn-primary" href="{{ route('requisiciones.detalles.almacen',['requisicion' => $requisitionID, 'detail' => $requisitionDetail->id]) }}">Detalles</a>--}}
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <a class="btn btn-default" href="{{ route('requisiciones.index') }}">Regresar</a>
    <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
  </div>
</div>

@endsection