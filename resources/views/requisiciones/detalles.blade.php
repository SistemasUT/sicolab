@extends('dashboard')

@section('content')

<script src="{{ asset('js/app.js') }}"></script>

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Detalles de material</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <!--Formulario de la requisición-->
    @if($requisitionsDetails->status_id == 3)
      <form method="post" action="{{ route('requisiciones.almacen.guardar') }}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="requisition_id" value="{{ $idRequisition }}">
        <input type="hidden" name="detail_id" value="{{ $idDetail }}">
        {{--<input type="hidden" name="material_id" value="{{ $requisitionsDetails->material_id }}">--}}    
        <table id="requisitionsTable" class="table table-striped">
          <thead>
              <tr>
                <td>ID</td>
                <td>Nombre del material </td>
                <td>Número de serie</td>
                <td>Fecha de expiración</td>
              </tr>
          </thead>
          <tbody>
            @for ($i = 0; $i < $requisitionsDetails->storage; $i++)
              <tr>
                <td></td>
                <td>
                  <select class="form-control" name="material_id[]">
                    @foreach($materials as $material)
                      @if($material->id == $requisitionsDetails->material_id)
                        <option selected value="{{ $material->id }}">{{ $material->type->name }}</option>
                      @else
                        <option value="{{ $material->id }}">{{ $material->type->name }}</option>
                      @endif
                    @endforeach
                  </select>
                </td>
                <td><input type="text" class="form-control" name="serial[]" /></td>
                <td><input type="date" class="form-control" name="expiration[]" /></td>
              </tr>
            @endfor
          </tbody>
        </table>
        <a class="btn btn-default" href="{{ url('/requisiciones/'.$idRequisition) }}">Regresar</a>
        <button type="submit" class="btn btn-primary">Almacenar</button>
      </form>
    @elseif($requisitionsDetails->status_id == 9)
      <table id="requisitionsTable" class="table table-striped">
        <thead>
            <tr>
              <td>ID</td>
              <td>Nombre del material </td>
              <td>Número de serie</td>
              <td>Unidad</td>
              <td>Marca</td>
              <td>Modelo</td>
            </tr>
        </thead>
        <tbody>
          @foreach($stockItems as $stock)
            <tr>
              <td></td>
              <td>{{ $stock->material->type->name }}</td>
              <td>{{ $stock->serial }}</td>
              <td>{{ $stock->material->unit->name }}</td>
              <td>{{ $stock->material->brand->name }}</td>
              <td>{{ $stock->material->model }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
      <a class="btn btn-default" href="{{ url('/requisiciones/'.$idRequisition) }}">Regresar</a>
    @endif
  </div>
</div>

@endsection