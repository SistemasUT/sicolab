@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Requisiciones</h3>
    <div class="box-tools pull-right">
      <a class="btn btn-seccess fa fa-plus" href="{{ route('requisiciones.crear') }}"> Agregar</a>
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table class="table table-striped">
      <thead>
          <tr>
            <td>ID</td>
            {{--<td>Área</td>--}}
            <td>Solicitante</td>
            <td>Concepto</td>
            {{--<td>Evento</td>--}}
            <td>Fecha de solicitud</td>
            <td>Folio</td>
            <td>Status</td>
            <td>Acciones</td>
          </tr>
      </thead>
      <tbody>
        @foreach($requisitions as $requisition)
          <tr>
            <td>{{$requisition->id}}</td>
            {{--<td>{{$requisition->area->name}}</td>--}}
            <td>{{$requisition->user->name}}</td>
            <td>{{$requisition->concept}}</td>
            {{--<td>{{$requisition->event}}</td>--}}
            <td>{{\Carbon\Carbon::parse($requisition->created_at)->format('d-m-Y H:i')}}</td>
            <td>{{$requisition->folio}}</td>
            @if($requisition->status->id == 1)
              <td>
                <span class="label label-primary">{{$requisition->status->name}}</span>
              </td>
            @elseif($requisition->status->id == 2)
              <td>
                <span class="label label-primary">{{$requisition->status->name}}</span>
              </td>
            @else
              <td>
                <span class="label label-danger">{{$requisition->status->name}}</span>
              </td>
            @endif
            <td>
              <form action="{{ route('requisiciones.eliminar', $requisition->id) }}" method="POST">
                @if($requisition->status->id == 1)
                  <a class="btn btn-primary fa fa-send" href="{{ route('requisiciones.enviar',$requisition->id) }}"></a>
                  <a class="btn btn-success fa fa-cart-plus" href="{{ route('requisiciones.asignar',$requisition->id) }}"></a>
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger fa fa-trash" type="submit"></button>
                @elseif($requisition->status->id == 2)
                  <a class="btn btn-primary fa fa-eye" href="{{ route('requisiciones.imprimir',$requisition->id) }}"></a>
                  <a class="btn btn-primary fa fa-reply" href="{{ route('requisiciones.recepcionar',$requisition->id) }}"></a>
                @else
                  <a class="btn btn-primary fa fa-eye" href="{{ route('requisiciones.imprimir',$requisition->id) }}"></a>
                @endif
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    {{--{!! $materials->links() !!}--}}
  </div>
</div>

@endsection