@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Requisiciones</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table class="table table-striped">
      <thead>
          <tr>
            <td>ID</td>
            {{--<td>Área</td>--}}
            <td>Solicitante</td>
            <td>Concepto</td>
            {{--<td>Evento</td>--}}
            <td>Fecha de solicitud</td>
            <td>Folio</td>
            <td>Status</td>
            <td>Acciones</td>
          </tr>
      </thead>
      <tbody>
        @foreach($requisitions as $requisition)
          <tr>
            <td>{{$requisition->id}}</td>
            {{--<td>{{$requisition->area->name}}</td>--}}
            <td>{{$requisition->user->name}}</td>
            <td>{{$requisition->concept}}</td>
            {{--<td>{{$requisition->event}}</td>--}}
            <td>{{\Carbon\Carbon::parse($requisition->created_at)->format('d-m-Y H:i')}}</td>
            <td>{{$requisition->folio}}</td>
            <td>
              @if($requisition->status_id == 2)
                <span class="label label-primary">{{$requisition->status->name}}</span>
              @else
                <span class="label label-danger">{{$requisition->status->name}}</span>
              @endif              
            </td>
            <td>
              {{--<form action="{{ route('requisiciones.eliminar', $requisition->id) }}" method="POST">--}}
                  {{--<a class="btn btn-primary fa fa-send" href="{{ route('requisiciones.enviar',$requisition->id) }}"></a>--}}
              <a class="btn btn-primary fa fa-eye" href="{{ route('requisiciones.asignar',$requisition->id) }}"></a>
              <a class="btn btn-default fa fa-qrcode" href="{{ route('requisiciones.download',$requisition->id) }}"></a>
                {{--@csrf
                @method('DELETE')
                  <button class="btn btn-danger fa fa-trash" type="submit"></button>
              </form>--}}
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    {{--{!! $materials->links() !!}--}}
  </div>
</div>

@endsection