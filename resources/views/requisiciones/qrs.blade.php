<!DOCTYPE html>
<html>
<head>
  <title>Códigos QR</title>
</head>
<body>
  <div class="row">
    @foreach($stockItems as $item)
      <img src="data:image/png;base64, 
        {!! base64_encode(QrCode::format('png')
          ->encoding('UTF-8')
          ->size(150)
          ->generate(
            'Nombre del material: '.$item->material->type->name.', '.
            'Número de serie: '.$item->serial.' , '.
            'Unidad de medida: '.$item->material->unit->name.', '.
            'Marca: '.$item->material->brand->name.' , '.
            'Modelo: '.$item->material->model)
        )!!} ">
      </br>
      <strong>N/S: </strong>{{$item->serial}}    
    @endforeach
  </div>
</body>
</html>