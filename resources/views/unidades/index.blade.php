@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Catálogo de Unidades</h3>
    <div class="box-tools pull-right">
      <a class="btn btn-success fa fa-plus" href="{{ route('unidades.create') }}"> Agregar</a>
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table class="table table-striped">
      <thead>
          <tr>
            <td>ID</td>
            <td>Nombre de la unidad</td>
            <td>Descripción</td>
            <td>Acciones</td>
          </tr>
      </thead>
      <tbody>
        @foreach($units as $unit)
          <tr>
            <td>{{$unit->id}}</td>
            <td>{{$unit->name}}</td>
            <td>{{$unit->description}}</td>
            <td>
              <form action="{{ route('unidades.destroy', $unit->id) }}" method="POST">
                <a class="btn btn-info fa fa-eye" href="{{ route('unidades.show',$unit->id) }}"></a>
                <a class="btn btn-primary fa fa-pencil" href="{{ route('unidades.edit',$unit->id) }}"></a>
                @csrf
                @method('DELETE')
                <button class="btn btn-danger fa fa-trash" type="submit"></button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    {!! $units->links() !!}
  </div>
</div>

@endsection