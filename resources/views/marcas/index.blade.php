@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Catálogo de Marcas</h3>
    <div class="box-tools pull-right">
      <a class="btn btn-success fa fa-plus" href="{{ route('marcas.create') }}"> Agregar</a>
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table class="table table-striped">
      <thead>
          <tr>
            <td>ID</td>
            <td>Nombre de la marca</td>
            <td>Descripción</td>
            <td>Acciones</td>
          </tr>
      </thead>
      <tbody>
        @foreach($brands as $brand)
          <tr>
            <td>{{$brand->id}}</td>
            <td>{{$brand->name}}</td>
            <td>{{$brand->description}}</td>
            <td>
              <form action="{{ route('marcas.destroy', $brand->id) }}" method="POST">
                <a class="btn btn-info fa fa-eye" href="{{ route('marcas.show',$brand->id) }}"></a>
                <a class="btn btn-primary fa fa-pencil" href="{{ route('marcas.edit',$brand->id) }}"></a>
                @csrf
                @method('DELETE')
                <button class="btn btn-danger fa fa-trash" type="submit"></button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    {!! $brands->links() !!}
  </div>
</div>

@endsection