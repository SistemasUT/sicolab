@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Catálogo de Categorías</h3>
    <div class="box-tools pull-right">
      <a class="btn btn-success fa fa-plus" href="{{ route('categorias.create') }}"> Agregar</a>
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table class="table table-striped">
      <thead>
          <tr>
            <td>ID</td>
            <td>Nombre de la categoría</td>
            <td>Descripción</td>
            <td>Acciones</td>
          </tr>
      </thead>
      <tbody>
        @foreach($categories as $category)
          <tr>
            <td>{{$category->id}}</td>
            <td>{{$category->name}}</td>
            <td>{{$category->description}}</td>
            <td>
              <form action="{{ route('categorias.destroy', $category->id) }}" method="POST">
                <a class="btn btn-info fa fa-eye" href="{{ route('categorias.show',$category->id) }}"></a>
                <a class="btn btn-primary fa fa-pencil" href="{{ route('categorias.edit',$category->id) }}"></a>
                @csrf
                @method('DELETE')
                <button class="btn btn-danger fa fa-trash" type="submit"></button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    {!! $categories->links() !!}
  </div>
</div>

@endsection