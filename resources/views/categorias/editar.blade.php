@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Modificar Categoría</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form method="post" action="{{ route('categorias.update', $category->id) }}">
      @method('PATCH')
      @csrf
      <div class="form-group">
        <label for="name">Nombre de la marca:</label>
        <input type="text" class="form-control" name="name" value="{{ $category->name }}" />
      </div>
      <div class="form-group">
        <label for="description">Descripción:</label>
        <textarea class="form-control" rows="5" name="description">{{ $category->description }}</textarea>
      </div>
      <a class="btn btn-default" href="{{ route('categorias.index') }}"> Cancelar</a>
      <button type="submit" class="btn btn-primary">Actualizar</button>
    </form>
  </div>
</div>

@endsection