@extends('dashboard')

@section('content')

<div class="box box-solid box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Crear Categoría</h3>
    <div class="box-tools pull-right">
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form method="post" action="{{ route('categorias.store') }}">
      <div class="form-group">
        @csrf
        <label for="name">Nombre de la categoría:</label>
        <input type="text" class="form-control" name="name"/>
      </div>
      <div class="form-group">
        <label for="description">Descripción:</label>
        <textarea class="form-control" rows="5" name="description"></textarea>
      </div>
      <a class="btn btn-default" href="{{ route('categorias.index') }}"> Cancelar</a>
      <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
  </div>
</div>

@endsection